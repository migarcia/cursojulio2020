const { createStore } = require("redux");

const INICIAL = {
    lista: []
}
/**
 * creamos reducer
 */
function anotaEnLista(estado = INICIAL, action) {
    let lista = estado.lista;
    if (action.type == "ADD") {
        lista.push(action.valor);
    }
    return Object.assign({},estado,{lista:lista});
}

/**
 * creamos una accion
 */
const CABECERA = {
    type: "ADD",
    valor: "Primer añadido"
}

/** preparamos una funcion que cree accion */
function addTarea(texto) {
    return {
        type: "ADD",
        valor: texto
    };
}

/**
 * creamos el store
 * declarando cual es su funcion reducer
 */
const almacen = createStore(anotaEnLista);

/**
 * lo hacemos funcionar
 */
console.log("Antes:", almacen.getState().lista);
almacen.dispatch(CABECERA);
console.log("Despues:", almacen.getState().lista);
almacen.dispatch(addTarea("Una orden de trabajo"));
console.log("siguiente:", almacen.getState().lista);
