function createXHR() {
	var request = false;
	try {
		request = new ActiveXObject('Msxml2.XMLHTTP');
	} catch (err2) {
		try {
			request = new ActiveXObject('Microsoft.XMLHTTP');
		} catch (err3) {
			try {
				request = new XMLHttpRequest();
			} catch (err1) {
				request = false;
			}
		}
	}
	return request;
}
/**
 * Rutina para enviar/recibir informacion a un server.
 * Devuelve una promesa a la que se le pueden colgar los .then/.catch necesarios
 * 
 * @param {obligatorio, string} url : Direccion del server remoto
 * @param {opcional, asume GET} metodo : Metodo a utilizar (GET, POST, cualquiera valido);
 * @param {opcional, asume NULL} json Objeto a envia, formato JSON
 */
function envia(url,metodo="GET",json=null) {
    // Return a new promise.
    return new Promise(function(resolve, reject) {
      // Do the usual XHR stuff
      var req = createXHR();
      req.open(metodo, url);
      req.setRequestHeader("Content-Type", "application/json");
      

      req.onload = function() {
        // This is called even on 404 etc
        // so check the status
        if (req.status == 200) {
          // Resolve the promise with the response text
          resolve(req.response);
        }
        else {
          // Otherwise reject with the status text
          // which will hopefully be a meaningful error
          reject(Error(req.statusText));
        }
      };

      // Handle network errors
      req.onerror = function() {
        reject(Error("Network Error"));
      };

      console.log(JSON.parse(json));
      // Make the request
      req.send(json);
    });
  }
  function get(url,metodo="GET",json=null){
    return envia(url,metodo,json);
  }