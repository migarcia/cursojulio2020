/**
 * Recibe un contenido que se ha de situar en un td
 * devuelve el td montado
 */
function creaTD(contenido) {
    let td = document.createElement("td");
    if (contenido instanceof HTMLElement) {
        td.appendChild(contenido);
    } else {
        td.appendChild(document.createTextNode(contenido));
    }
    return td;
}
/**
 * Recibe una lista con todo lo que se debe incluir en un tr
 * devuelve el tr formado
 * @param {array} listaContenido 
 */
function creaTR(...listaContenido) {
    let tr = document.createElement("tr");
    listaContenido.forEach(contenido => tr.appendChild(creaTD(contenido)));
    return tr;
}