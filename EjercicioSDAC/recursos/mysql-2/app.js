const mysql = require('mysql');
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'tienda'
}); 
connection.connect((err) => {
  if (err) throw err;
  console.log('Connected!');
});

connection.query('SELECT * FROM c_pais', (err,rows) => {
  if(err) throw err;
 
  console.log('Data received from Db:\n');
  console.log(rows);



  rows.forEach( (row) => {
    console.log(`${row.country_id} \t ${row.short_name}`); 
  });
}); 
 
//*********************añadiendo producto */
const cate = { 
  id_categoria: 0,
  cat_nombre: 'Inciensos',
  cat_descripcion: 'Incienso cuidadosamente fabricado'
 };
 connection.query('INSERT INTO categoria SET ?', cate, (err, res) => {
  if(err) throw err;

  console.log('Last insert ID:', res.insertId); 
});

/***********************************************************Actualizando */
connection.query(
  'UPDATE categoria SET cat_descripcion = ? Where id_categoria = ?',
  ['Gran seleccion de incienso', 1],
  (err, result) => {
    if (err) throw err;

    console.log(`Cambiados ${result.changedRows} row(s)`);
  }
);

/****************************************************Borrado */
connection.query(
  'DELETE FROM categoria WHERE id_categoria = ?', [1], (err, result) => {
    if (err) throw err;

    console.log(`Borrado ${result.affectedRows} row(s)`);
  }
);
 
/*********************uso directo, esquivando  SQL injection 

connection.query(
  `SELECT * FROM categoria WHERE id_categoria = ${mysql.escape(userLandVariable)}`,
  function(err, rows){ ... }
); 
*/