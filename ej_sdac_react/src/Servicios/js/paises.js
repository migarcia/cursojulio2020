import React, { Component } from 'react';

//******* Area de control *******************
const API_URL = "http://localhost:3000/api/cp_pais/";   //direccion base
const LPPAGINA = "20";                              //lineas por pagina
const INS = "Añadir"    
const MOD = "Modificar"
const DEL = "Delete"
const TAG_PAGINACION = "paginas"                    // id en donde se hubican la paginacion
const NOMBRE_FORMULARIO = "formularioMantenimiento"  //nombre formulario de mantenimiento

export default class P
function presentaLista(respuesta) {
    let tbody = document.getElementById("tablaBody");
    tbody.innerHTML = "";
    respuesta.forEach(row => {
        
        // tr = document.createElement("tr");
        // td = document.createElement("td");
        // td.appendChild(document.createTextNode(row.country_id));
        // tr.appendChild(td);
        // td = document.createElement("td");
        // td.appendChild(document.createTextNode(row.iso2));
        // tr.appendChild(td);
        // td = document.createElement("td");
        // td.appendChild(document.createTextNode(row.short_name));
        // tr.appendChild(td);
        // td = document.createElement("td");
        // td.appendChild(document.createTextNode(row.spanish_name));
        // tr.appendChild(td);
        // td = document.createElement("td");
        // td.appendChild(document.createTextNode(row.numcode));
        // tr.appendChild(td);
        // td = document.createElement("td");
        eaE = document.createElement("a");
        eaE.setAttribute("href", "#");
        eaE.setAttribute("onclick", "gestionaUno('E','" + row.country_id + "')");
        eaE.setAttribute("data-toggle","modal");
        eaE.setAttribute("data-target","#" + NOMBRE_FORMULARIO);
        eaE.appendChild(document.createTextNode("Edit"));
        // td.appendChild(eaE);
        // tr.appendChild(td);
        // td = document.createElement("td");
        eaD = document.createElement("a");
        eaD.setAttribute("href", "#");
        eaD.setAttribute("onclick", "gestionaUno('D','" + row.country_id + "')");
        eaD.setAttribute("data-toggle","modal");
        eaD.setAttribute("data-target","#" + NOMBRE_FORMULARIO);
        eaD.appendChild(document.createTextNode("Delete"));
        // td.appendChild(ea);
        // tr.appendChild(td);
        // tbody.appendChild(tr);
        tbody.appendChild(creaTR(row.country_id,row.iso2,row.short_name,row.spanish_name,row.numcode,eaE,eaD));

    })
}

function presentaUno(op,datos){
    console.log(op , datos);
    document.getElementById("country_id").value = datos.country_id;
    document.getElementById("short_name").value = datos.short_name;
    document.getElementById("spanish_name").value = datos.spanish_name;
    document.getElementById("calling_code").value = datos.calling_code;
    document.getElementById("cctld").value = datos.cctld;
    document.getElementById("iso3").value = datos.iso3;
    document.getElementById("long_name").value = datos.long_name;
    document.getElementById("numcode").value = datos.numcode;
    //control boton
    mensaje = INS;
    if (op == "E") mensaje=MOD;
    if (op == "D") mensaje=DEL;
    boton = document.getElementById("botonEnvio");
    boton.setAttribute("onclick","recogerDatos('" + op + "')" );
    boton.setAttribute("data-toggle","modal");
    boton.setAttribute("data-target","#" + NOMBRE_FORMULARIO);
    boton.innerHTML = mensaje;
}
function recogerDatos(op){
    objeto = {
        country_id:  document.getElementById("country_id").value,
        short_name: document.getElementById("short_name").value  ,
        spanish_name: document.getElementById("spanish_name").value  ,
        calling_code: document.getElementById("calling_code").value  ,
        cctld : document.getElementById("cctld").value  , 
        iso3: document.getElementById("iso3").value  ,
        long_name:document.getElementById("long_name").value  ,
        numcode:document.getElementById("numcode").value  ,
    }
    enviaFormulario(op,objeto);
}


