import React from "react";
import { API_URL, LPPAGINA, INS, MOD, DEL, TAG_PAGINACION, NOMBRE_FORMULARIO }
    from '../../Servicios/js/Constantes';
import Comunicaciones from './comunicaciones';


export default class FuncionesSDCA {

    clasificacion = ""; //clasificar por campo          //campo a clasificar
    clasInversa = false;                                //Ascendente/desc
    paginaActual = 0                                    //pagina actual



    /**
     *  
     * El fichero que utilice estas funciones, debe implementar los siguientes metodos
     *  
     * presentaLista   - recibe un array con todas las rows, y las puede nontar en pantalla
     * presentaUno     - recibe un objeto con un registro, y lo presenta en pantalla
     */

    /**
     * Recibe la respuesta de un count y genera los enlaces de paginacion
     * en una etiqueta del HTML definida en TAG_PAGINACION
     * Depende tambien de la constante de lineas por pagina (LPPAGINA)
     * 
     * @param {} respuesta  Resultado de la llamada con la extension count (xmysql)
     */

    static obtenDatos(respuesta) {

        let registros = respuesta[0].no_of_rows;
        console.log(registros);
        let cant = registros / LPPAGINA;
        let salida = "";
        for (let a = 1; a < cant; a++) {
            salida += <span onClick={() => this.irAPagina(a)}></span>
        }
        return salida;
    }

    //-----------------------------------------------------
    //     guarda pagina que se solicita
    //-----------------------------------------------------
    static irAPagina(pagina) {
        if (pagina != paginaActual) {
            paginaActual = pagina;
            leerDatos(API_URL);
        }
    }


    //----------------------------------------
    //   guarda campo de clasificacion
    //----------------------------------------
    static listadoClasificado(motivo) {
        if (clasificacion == motivo) {
            clasInversa = !clasInversa;
        } else {
            clasificacion = motivo;
            clasInversa = false;
        }
        leerDatos(API_URL);
    }


    /**
     * 
     * @param {*} parametros lista de parametros montada
     * @param {*} nombrePar Parametro a añadir
     * @param {*} valor Valor del parametro a añadir
     */
    static montaParam(parametros, nombrePar, valor) {
        if (parametros != "") parametros += "&"
        parametros += nombrePar;
        parametros += "=";
        parametros += valor;
        return parametros
    }

    static preparaParams(parametros) {
        parametros = montaParam(parametros, "_size", LPPAGINA);
        if (clasificacion != "") {
            parametros = montaParam(parametros, "_sort", (clasInversa ? "-" : "") + clasificacion);
        }
        if (paginaActual != 0) {
            parametros = montaParam(parametros, "_p", paginaActual);
        }
        return parametros;
    }
    static leerDatos(url, parametros = "") {
        parametros = preparaParams(parametros);
        this.envia(url + "?" + parametros, "GET", null)
            .then(function (response) {
                this.presentaLista(JSON.parse(response));
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    static gestionaUno(op, id) {
        envia(API_URL + id)
            .then(function (response) {

                presentaUno(op, JSON.parse(response)[0]);
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    static enviaFormulario(op, objeto) {
        orden = "POST";
        id = "";
        if (op == "E") { orden = "PATCH"; id = objeto.country_id; }
        if (op == "D") { orden = "DELETE"; id = objeto.country_id; }
        //console.log(orden,objeto);
        envia(API_URL + id, orden, JSON.stringify(objeto))
            .then(function (response) {
                leerDatos(API_URL);
            })
            .catch(function (error) {
                console.error(error);
            });
    }
    static gestionaUnoDynamic(op, id) {
        datos = {
            "query":"SELECT * FROM cp_pais WHERW country_id = ??",
            "params": id
        }
        envia("http://localhost:3000/dynamic","GET",DOMSettableTokenList)
            .then(function (response) {

                presentaUno(op, JSON.parse(response)[0]);
            })
            .catch(function (error) {
                console.error(error);
            });
    }
}