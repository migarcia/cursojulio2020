import React, { Component } from 'react';
import { Link } from "react-router-dom";


export default class Menu extends Component {


    render() {

        return (
            <div className="container-fluid">
                <div className="row">
                    <nav id="sidebarMenu" className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                        <div className="sidebar-sticky pt-3">
                            <ul className="nav flex-column">
                                <li className="nav-item">
                                    <Link className="nav-link active" to="../">
                                        <span data-feather="home"></span> Home
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/Paises/listadoPaises.html">
                                        <span data-feather="file"></span> Paises
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="#">
                                        <span data-feather="file"></span> Comunidades Autonomas
                            </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="#">
                                        <span data-feather="shopping-cart"></span> Provincias
                            </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="#">
                                        <span data-feather="users"></span> Poblaciones
                            </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="#">
                                        <span data-feather="bar-chart-2"></span> Tipos de Clientes
                            </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="#">
                                        <span data-feather="layers"></span> Tipos de Proveedor
                            </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="#">
                                        <span data-feather="bar-chart-2"></span> Tipos facturas Clientes
                            </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="#">
                                        <span data-feather="layers"></span> Tipos Facturas Proveedor
                            </Link>
                                </li>
                            </ul>


                        </div>
                    </nav>

                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                    </main>
                </div>
            </div>
        )
    }
}