
import React, { Component } from 'react';

import { Switch, Route } from "react-router-dom";
import ListadoPaises from './../Paginas/ListadoPaises';

export default class Trabajo extends Component {

    render() {
        return (
            <Switch>
 
                <Route path="/Paises/listadoPaises.html" component={ListadoPaises} />
 
            </Switch>
        )
    }
}