import React, { Component } from 'react';
import { Switch, Route } from "react-router-dom";
import Comunicaciones from '../../Servicios/js/comunicaciones';

import {API_URL,LPPAGINA,INS, MOD,DEL,TAG_PAGINACION, NOMBRE_FORMULARIO} 
        from '../../Servicios/js/Constantes';


export default class ListadoPaises extends Component {
    clasificacion = ""; //clasificar por campo          //campo a clasificar
    clasInversa = false;                                //Ascendente/desc
    paginaActual = 0                                    //pagina actual

    constructor(props) {
        super(props);
        Comunicaciones.envia(API_URL + "count")              //consiguir el numero de registros
        .then(function (response) {
            this.obtenDatos(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });
    ListadoPaises.leerDatos(API_URL);                 //leer datos de tabla
    }

    montaLinea(row) {
        return (
            <tr>
                <td>row.country_id</td>
                <td>row.iso2</td>
                <td>row.short_name</td>
                <td>row.numcode</td>
                <td>row.country_id</td>
                <td>row.country_id</td>
                <td onClick={this.gestionaUno('E', row.country_id)}>Edit</td>
                <td onClick={this.gestionaUno('D', row.country_id)}>Delete</td>
            </tr>
        )
    }
    render() {
        let respuesta = this.setState.respuesta;
        const url = this.props.match.url;
        
        return (
            <div>
                <h1>Listado de paises <a href="#" data-toggle="modal" data-target="#modalContactForm"> +</a></h1>
                <table className="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col"><a href="#" onclick="listadoClasificado('iso2')">ISO2</a></th>
                            <th scope="col"><a href="#" onclick="listadoClasificado('short_name')">Nombre corto</a></th>
                            <th scope="col"><a href="#" onclick="listadoClasificado('spanish_name')">Nombre
                                    Castellano</a></th>
                            <th scope="col"><a href="#" onclick="listadoClasificado('numcode')">Codigo</a></th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delet</th>
                        </tr>
                    </thead>
                    <tbody id="tablaBody">
                    {respuesta.forEach(row => this.montaLinea(row))}

                    </tbody>
                </table>
            </div>
        )
    }
    /**
     * Recibe la respuesta de un count y genera los enlaces de paginacion
     * en una etiqueta del HTML definida en TAG_PAGINACION
     * Depende tambien de la constante de lineas por pagina (LPPAGINA)
     * 
     * @param {} respuesta  Resultado de la llamada con la extension count (xmysql)
     */

    static obtenDatos(respuesta) {

        let registros = respuesta[0].no_of_rows;
        console.log(registros);
        let cant = registros / LPPAGINA;
        let salida = "";
        for (let a = 1; a < cant; a++) {
            salida += <span onClick={() => this.irAPagina(a)}></span>
        }
        return salida;
    }

    //-----------------------------------------------------
    //     guarda pagina que se solicita
    //-----------------------------------------------------
    static irAPagina(pagina) {
        if (pagina != this.paginaActual) {
            this.paginaActual = pagina;
            this.leerDatos(API_URL);
        }
    }


    //----------------------------------------
    //   guarda campo de clasificacion
    //----------------------------------------
    static listadoClasificado(motivo) {
        if (this.clasificacion == motivo) {
            this.clasInversa = !this.clasInversa;
        } else {
            this.clasificacion = motivo;
            this.clasInversa = false;
        }
        this.leerDatos(API_URL);
    }


    /**
     * 
     * @param {*} parametros lista de parametros montada
     * @param {*} nombrePar Parametro a añadir
     * @param {*} valor Valor del parametro a añadir
     */
    static montaParam(parametros, nombrePar, valor) {
        if (parametros != "") parametros += "&"
        parametros += nombrePar;
        parametros += "=";
        parametros += valor;
        return parametros
    }

    static preparaParams(parametros) {
        parametros = this.montaParam(parametros, "_size", LPPAGINA);
        if (this.clasificacion != "") {
            parametros = this.montaParam(parametros, "_sort", (this.clasInversa ? "-" : "") + this.clasificacion);
        }
        if (this.paginaActual != 0) {
            parametros = this.montaParam(parametros, "_p", this.paginaActual);
        }
        return parametros;
    }
    static leerDatos(url, parametros = "") {
        parametros = this.preparaParams(parametros);
        Comunicaciones.envia(url + "?" + parametros, "GET", null)
            .then(function (response) {
                this.presentaLista(JSON.parse(response));
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    static gestionaUno(op, id) {
        Comunicaciones.envia(API_URL + id)
            .then(function (response) {

                this.presentaUno(op, JSON.parse(response)[0]);
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    static enviaFormulario(op, objeto) {
        let orden = "POST";
        let id = "";
        if (op == "E") { orden = "PATCH"; id = objeto.country_id; }
        if (op == "D") { orden = "DELETE"; id = objeto.country_id; }
        //console.log(orden,objeto);
        Comunicaciones.envia(API_URL + id, orden, JSON.stringify(objeto))
            .then(function (response) {
                this.leerDatos(API_URL);
            })
            .catch(function (error) {
                console.error(error);
            });
    }
}
