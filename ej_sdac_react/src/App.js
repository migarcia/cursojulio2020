import React from 'react';

import Cabecera from './Components/Comun/Cabecera';
import Menu from './Components/Comun/Menu';
import Trabajo from './Components/Comun/Trabajo';

function App() {
  return (
    <div >
      <Cabecera />
      <Menu />
      <Trabajo/>
    </div>
  );
}

export default App;
