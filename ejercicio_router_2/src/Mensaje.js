import React from 'react';
import PropTypes from 'prop-types';

export default function Mensaje(props) {
    return (
        <div>
        {props.pruebas}
            {props.children}
        </div>
    )
}

Mensaje.propTypes = {
    children: PropTypes.string.isRequired,
    pruebas:PropTypes.string.isRequired
  };
