import React,{Component} from "react";
import Previsio5dias from "./components/Previsio5dias";
import PrevisioSemanal from "./PrevisioSemanal";
import Previsio from "./Previsio";



class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <PrevisioSemanal />
        <Previsio5dias />
        
      </div>
    );
  }
}
 
export default App;