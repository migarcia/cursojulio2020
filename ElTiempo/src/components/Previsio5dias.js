import React from 'react';
//instrucciones en 
//https://openweathermap.org/forecast5

import { WEATHER_APIKEY } from './keys';
import CuadroTiempo from './CuadroTiempo';
import "./estilo.css";
import NewWindow  from 'react-new-window';

export default class PrevisioSemanal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list: []
        };
        this.getPrevisio = this.getPrevisio.bind(this);


    }
    componentDidMount() {
        this.getPrevisio();
    }

    getPrevisio() {
        const ciutat = "barcelona,es";
        const apiKey = WEATHER_APIKEY    //"16aaaa97cfb7e5d9bcccc9e533025f9b";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState(data))
            .catch(error => console.log(error));
    }

    detalle = (e,l) => {
        console.log(e);
        this.setState({listar:e})
    }
    render() {
        if (!this.state.list.length) {
            return <h1 > Cargando datos... </h1>
        }
        let dia = {
            num: 0,
            fecha: null,
            max: 0,
            min: 0,
            nubes: 0
        }
        let lista5 = [];
        this.state.list.forEach((item, i) => {
            let d = new Date(item.dt * 1000);
            let fecha = d.getDate() + '/' + (parseInt(d.getMonth()) + 1) + '/' + d.getFullYear()
            if (dia.fecha !== fecha) {
                if (dia.fecha != null) {
                    lista5.push(dia)
                }
                dia = {
                    num: i,
                    fecha: fecha,
                    fecval: item.dt * 1000,
                    max: item.main.temp_max,
                    min: item.main.temp_min,
                    nubes: item.clouds.all,
                    icon: item.weather[0].icon
                }
            } else {
                if (dia.max < item.main.temp_max) dia.max = item.main.temp_max;
                if (dia.min > item.main.temp_min) dia.min = item.main.temp_min
            }
        })
        lista5.push(dia)
        console.log(lista5);


        return (
            <div className="informe">
                {lista5.map((dato, index) => (
                    <CuadroTiempo valor={dato} index={index} salto={this.detalle}/>

                ))}
            </div>)

    }
}