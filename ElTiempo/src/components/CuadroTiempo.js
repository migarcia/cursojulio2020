import React,{Component} from 'react';

export default class CuadroTiempo extends Component{

    render()
    {
        const DIASEM = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
        return(
            <div className="caja" key={this.props.index} onClick={e=>this.props.salto(this.props.valor.num)}>
                
                {DIASEM[(new Date(this.props.valor.fecval).getDay())]}<br/>
                <img src={"http://openweathermap.org/img/w/" + this.props.valor.icon + ".png"} />
                <p>{this.props.valor.max}   /   {this.props.valor.min}</p>

            </div>
        )
    }
}