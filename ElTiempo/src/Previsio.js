import React from 'react';

export default class Previsio extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list: []
        };
        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();

    }
    getPrevisio() {
        const ciutat = "barcelona,es";
        const apiKey = "16aaaa97cfb7e5d9bcccc9e533025f9b";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState(data))
            .catch(error => console.log(error));
    }

    render() {
        if (!this.state.list.length) {
            return <h1 > Cargando datos... </h1>
        }

        let fecha = new Date(this.state.list[0].dt * 1000);
        let fecha2 = new Date(this.state.list[1].dt * 1000);
        console.log(this.state.list);

        return (
            <div>
                <h3> Prevision para {fecha.toString()} </h3>
                <h3 > Temperatura {this.state.list[0].main.temp} </h3>

                <h3 > Prevision para {fecha2.toString()} </h3>
                <h3 > Temperatura {this.state.list[1].main.temp} </h3>

            </div>)
                                }
}