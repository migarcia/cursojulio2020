import React from 'react';
//instrucciones en 
//https://openweathermap.org/forecast5
export default class PrevisioSemanal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list: []
        };
        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();

    }
    getPrevisio() {
        const ciutat = "barcelona,es";
        const apiKey = "16aaaa97cfb7e5d9bcccc9e533025f9b";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;
 
        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState(data))
            .catch(error => console.log(error));
    }

    render() {
        if (!this.state.list.length) {
            return <h1 > Cargando datos... </h1>
        }
        console.log(this.state.list);

        return (
            <table>
                <thead>
                    <tr>

                        <th>dt_txt</th>
                        <th>main.temp</th>
                        <th>weather.icon</th>
                        <th>clouds.all</th>
                        <th>wind.speed</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.list.map((dato, index) => (

                        <tr key={index}>
                            <td>{dato.dt_txt}</td>
                            <td>{dato.main.temp}</td>
                            <td>{dato.weather.icon}</td>
                            <td>{dato.clouds.all}</td>
                            <td>{dato.wind.speed}</td>
                        </tr>
                    ))}
                </tbody>
            </table>)
    }
}