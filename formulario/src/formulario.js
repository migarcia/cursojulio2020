import React, { Component } from 'react';


class Formulario extends Component {
    constructor(props){
        super(props)
        this.state = {
            nombre:null,
            situ:null
        }
    }

   
     procesaInput = e => {
        console.log(e.target.value)
        this.setState ({nombre:e.target.value})
     }
    ;
    muestra = () => this.setState ({situ:true})
    limpia = () => this.setState ({situ:false})

    render() {
        return (
            this.state.situ ?
            <div>Hola {this.state.nombre}
            <button onClick={this.limpia}>Limpiar</button>
            </div>
            :
            <div>
                <input type="text" onChange ={(e) => {this.procesaInput(e)}}/>
                <button onClick={this.muestra} >Enviar</button>
            </div>
        )
    }
}

export default Formulario;