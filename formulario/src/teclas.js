import React from 'react'
import PropTypes from 'prop-types'

function teclas({text}) {
    return (
        <div>
            <h1>{text}</h1>
        </div>
    )
}

teclas.propTypes = {
    text: PropTypes.string,
}

export default teclas

