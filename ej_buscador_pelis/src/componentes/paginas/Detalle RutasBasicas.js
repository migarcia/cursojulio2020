import React, { Component } from 'react'

const API_KEY = '956aa0ac';
let API_PELI = `http://www.omdbapi.com/?&apikey=${API_KEY}`;


export class Detalle extends Component {

    state = {
        pelicula: {}
    }

    _leerPelicula = (id) => {

        const ruta = API_PELI + `&i=${id}`
        console.log(ruta)
        fetch(ruta)
            .then(res => res.json())
            .then(pelicula => {
                console.log(pelicula)
                this.setState({pelicula})

            })
    }
    _goBack(){
        window.history.back();
    }

    componentDidMount() {
        const id = this.props.id;
        this._leerPelicula(id);

    }
    render() {
        const { Title, Poster, Actors, Metascore, Plot } = this.state.pelicula;
        return (
            <div>
                <button onClick={this._goBack}>Volver</button>
                <h1>{Title}</h1>
                <img src={Poster} />
                <h3>{Actors}</h3>
                <span>{Metascore}</span>
                <p>{Plot}</p>
            </div>
        )
    }
}