import React, { Component } from 'react'

import { Titulo } from './Titulo'
import { FormBuscador } from './FornBuscador';
import { PresentaPelis } from './PresentaPelis';



export class GestorBuscador extends Component {

    state = { resultados: [], buscador: false, numero:0 };


    _handleContenido = (resultados,numero) => {
        this.setState({ resultados, buscador:true, numero:numero});
    } 
    _presentarBusqueda = (resultados) => {
        return this.state.resultados.length === 0
            ? <p> No hay coincidencias</p>
            : <PresentaPelis lista={this.state.resultados} cantidad={this.state.numero} />
    }
    render() {
       
        return (
            <div>
            
                <Titulo>Buscador de peliculas</Titulo>
                <div className="buscador">
                    <FormBuscador datos={this._handleContenido} />
                </div>
                {this.state.buscador
                    ? this._presentarBusqueda()
                    : <p> Indique la pelicula a buscar</p>}
            </div>
        )
    }
}