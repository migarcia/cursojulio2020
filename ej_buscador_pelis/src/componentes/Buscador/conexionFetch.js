class ConexionFetch {

    async function leer() {
    let url = 'https://someurl.com';
    let options = {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify({
            property_one: value_one,
            property_two: value_two
        })
    };
    let response = await fetch(url, options);
    let responseOK = response && response.ok;
    if (responseOK) {
        let data = await response.json();
        // do something with data
    }
}
}