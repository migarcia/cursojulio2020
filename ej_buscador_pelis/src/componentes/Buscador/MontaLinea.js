import React, { Component } from 'react'


export class MontaLinea extends Component {

    render() {
        const linea = this.props.linea
        return (
            <p key={linea.imdbID}>
            {linea.Title}
            </p>
        )
    }
}