import React, { Component } from 'react'

const API_KEY = '121adf75';
let API_PELI = `http://www.omdbapi.com/?&apikey=${API_KEY}`;


export class FormBuscador extends Component {

    state = {
        inputMovie: ''
    }

    _handleChange = (e) => {
        this.setState({ inputMovie: e.target.value })
    }

    _handleSubmit = (e) => {
        e.preventDefault();
        const { inputMovie } = this.state;
        const ruta = API_PELI + `&s=${inputMovie}`
        console.log(ruta)
        fetch(ruta)
            .then(res => res.json())
            .then(
                (results) => {
                    console.log(results);
                    const { Response = "", Search = [], totalResults } = results;

                    if (Response !== "False") {
                        this.props.datos(Search, totalResults);
                    }
                    /**** tratamiento errores de la aplicacion
                     * viene dentro del atributo Error
                     */

                },
                (error) => {                
                    this.props.datos([], 0);              
                }
            )
    }
    render() {
        return (
            <form onSubmit={this._handleSubmit}>
                <div className="field has-addons">
                    <div className="control">
                        <input
                            className="input"
                            type="text"
                            placeholder="Titulo de la pelicula"
                            onChange={this._handleChange} />
                    </div>
                    <div className="control">
                        <button className="button is-info">
                            Buscar
                    </button>
                    </div>
                </div>
            </form>
        )
    }
}
