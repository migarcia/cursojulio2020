import React, { Component } from 'react'

import { Titulo } from './Titulo'
import { FormBuscador } from './FornBuscador';
import { PresentaPelis } from './PresentaPelis';
import {Detalle} from '../paginas/Detalle';


 export class GestorBuscador_R extends Component {

    state = { resultados: [], buscador: false };


    _handleContenido = (resultados) => {
        this.setState({ resultados, buscador:true });
    }
    _presentarBusqueda = (resultados) => {
        return this.state.resultados.length === 0
            ? <p> No hay coincidencias</p>
            : <PresentaPelis lista={this.state.resultados} />
    }
    render() {
        /**
         * si necesitamos esa informacion sin rutas
         * podriamos hacer
         */
        const url = new URL(document.location);
        const hasId = url.searchParams.has('id');
        if (hasId){
            return <Detalle id={url.searchParams.get('id')}/>
        }
        //------------------------------------------------------
        return (
            <div>
                <Titulo>Buscador de peliculas</Titulo>
                <div className="buscador">
                    <FormBuscador onResult={this._handleContenido} />
                </div>
                {this.state.buscador
                    ? this._presentarBusqueda()
                    : <p> Indique la pelicula a buscar</p>}
            </div>
        )
    }
} 