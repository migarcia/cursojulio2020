import React, { Component } from 'react'


export class MontaPoster extends Component {

    render() {
        const linea = this.props.linea
        return (
            <a href={`?id=${linea.imdbID}`} className="card" key={linea.imdbID} >
                <div className="card-image">
                    <figure className="image ">
                        <img
                            src={linea.Poster}
                            alt={linea.Title}
                        />
                    </figure>
                </div>
                <div className="card-content">
                    <div className="media">
                        <div className="media-content">
                            <p className="title is-4"> {linea.Title}</p>
                            <p className="subtitle is-6">{linea.Year}</p>
                        </div>
                    </div>


                </div>
            </ a>
        )
    }
}