import React, { Component } from 'react'
//import { MontaLinea } from './MontaLinea';
import { MontaPoster } from './MontaPoster';


export class PresentaPelis extends Component {



    render() {

        const lista = this.props.lista;
        const cantidad = this.props.cantidad;
        return (
            <div className='posterLista'>
                <h1 style={{width:"100%"}}>Se han encontrado {cantidad} películas</h1>
                
                {lista.map(pelicula => (
                    <div key={pelicula.imdbID} className='posterLista-item'>
                        <MontaPoster linea={pelicula} />
                    </div>
                ))}
            </div>
        )
    }
}