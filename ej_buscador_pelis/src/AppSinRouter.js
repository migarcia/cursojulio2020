import React from 'react';

import { GestorBuscador } from './componentes/Buscador/GestorBuscador'

import './App.css';
import 'bulma/css/bulma.css';


function AppSinRouter() {
  
  return (
    <GestorBuscador />
  );
}

export default App;
