import React from 'react';
import { BrowserRouter as Router} from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';

import { GestorBuscador } from './componentes/Buscador/GestorBuscador'
import { Detalle } from './componentes/paginas/Detalle';
import {NotFound } from './componentes/NotFound';

import './App.css';
import 'bulma/css/bulma.css';


function App() {
  
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={GestorBuscador} />
        <Route path='/detalle/:id/:name/:epoca' component={Detalle} />
        <Route path='/detalle/:id' component={Detalle} />
        <Route component= {NotFound} />
      </Switch>
    </Router>

  );
}

export default App;
