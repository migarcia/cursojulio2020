import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import { createStore } from "redux";
import anotaEnLista from "./Components/Redux/reducer";



/**
 * creamos el store
 * declarando cual es su funcion reducer
 */
const almacen = createStore(anotaEnLista);

/**
 * Encapsulamos con Provider para que todo react pueda ver el almacen
 */

ReactDOM.render(
    <Provider store={almacen}>
        <App />
    </Provider>
    , document.getElementById("root"));
