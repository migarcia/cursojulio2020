import React from "react";
import Form from './Components/Formulario1';
import ListadoTareas from './Components/listado/listadoTareas';

export default () => (
  <>
    <h1>Prueba de redux</h1>
    <ListadoTareas/>
    <Form/>
  </>
);
