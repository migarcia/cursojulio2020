import React from "react";
import { connect } from "react-redux";

const select = datos => {
  console.log('lista', datos)
  return { lista: datos.lista };
};

const ConnectedList = ({ lista }) => {
 
 return (
    <ul>
      {lista.map((el, i) => <li key={i}>{el}</li>) }

    </ul>
 )
};

const ListadoTareas = connect(select)(ConnectedList);
export default ListadoTareas;