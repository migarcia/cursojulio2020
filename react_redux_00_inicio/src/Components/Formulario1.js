import React, { Component } from "react";
import { connect } from "react-redux";
import {addTarea} from './Redux/Acciones'

function mapDispatchToProps(dispatch) {
    console.log('this.props');
    return   {  
        addTarea: datos => dispatch(addTarea(datos))
    }
    
}

class Formulario1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tarea: ""
        };
        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }

    _handleChange(event) {
        this.setState({ [event.target.id]: event.target.value })
        console.log('FormularioChange',this.state);
    }

    _handleSubmit(event) {
        event.preventDefault();
        const { tarea } = this.state;
        console.log('FormularioSubmit',tarea);
        console.log(this)
        this.props.addTarea(tarea);
        this.setState({ tarea: "" });
    }
    render() {
        const { tarea } = this.state;
        return (
             <form onSubmit={this._handleSubmit}>
                <div>
                    <label htmlFor="tarea">Tarea a realizar</label>
                    <input
                        type="text"
                        id="tarea"
                        value={tarea}
                        onChange={this._handleChange}
                    />
                </div>
                <button type="submit">Guardar</button>
        </form> 
        );
    }
}

const Form = connect(
    null,
    mapDispatchToProps
)(Formulario1);

export default Form;
