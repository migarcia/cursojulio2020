import {ADD} from "./../constantes/tipos-accion";
const estadoLista = {
    lista:[]
}

/**
 * creamos reducer
 */
function anotaEnLista(estado = estadoLista, action) {
    console.log('Anota en lista',action)
    let lista = estado.lista;
     if (action.type == ADD) {
        lista.push(action.valor);
    }
    return Object.assign({},estado,{lista:lista});
}

export default anotaEnLista;