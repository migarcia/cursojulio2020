import {ADD} from "../constantes/tipos-accion";
/**
 * declaracion de acciones
 * 
 * @param {} texto 
 * 
 * Retorna accion
 */

/** preparamos una funcion que cree accion */
export function addTarea(texto) {
    console.log('Añadiendo ----_>',texto);
    return {
        type: ADD,
        valor: texto
    };
}