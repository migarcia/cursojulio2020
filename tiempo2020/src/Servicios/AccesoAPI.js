import { WEATHER_APIKEY } from '../Constantes/keys';


/**
 * Acceso a la web https://www.openweathermap.com/ para recuperar
 * toda la informacion de fechas
 * Las llamadas son staticas y devuelven SIEMPRE Promise
 * Por ejemplo, en react, podriamos hacer:
 * AccesoAPI.leerForecast()
 *       .then(respuesta=>{
 *           this.setState({
 *               lista:respuesta,
 *           })
 */
const CIUDAD = "barcelona,es";
const API_KEY = WEATHER_APIKEY;    //"16aaaa97cfb7e5d9bcccc9e533025f9b";
const FORECAST = "forecast";       //Pronostico
const WEATHER = "weather";

const API_SER = "http://api.openweathermap.org/data/2.5/";
export default class AccesoAPI {

    static async leerTiempoActual(ciudad = null) {
        if (ciudad != null) {
            ciudad = CIUDAD;
        }
        let ruta = API_SER + WEATHER + '?APPID=' + API_KEY + '&units=metric';
        ruta += `&q=${ciudad}`;
        console.log(ruta);
        return this.accederApi(ruta);
    }


    /*
    *  Metodo de acceso a la API. Comun......
    */
    static async accederApi(ruta) {
        return await fetch(ruta)
            .then(res => res.json())
            .then(
                results => {
                    console.log(results);
                    return results;
                }
            )
    }


}