import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AccesoAPI from './../Servicios/AccesoAPI';


export default class PresentarTiempo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            recibido:false
        }
        let ciudad = null;
        ciudad=this.props.match.params.ciudad;
        AccesoAPI.leerTiempoActual(ciudad)
            .then(response => {
                this.setState(response);
                this.setState({recibido:true});
                })
            .catch(error => console.log(error));
    }

    render() {
        console.log(this.state);
        
        if (!this.state.recibido) {
            return <h1 > Cargando datos... </h1>
        } 
        const { list, main, weather, coord, name, sys} = this.state;
        console.log(list);
        return (
            <div>
                <h1>Presentar tiempo</h1>
                <p>Ciudad : {name} ({sys.country}) Situacion ( {coord.lon} , {coord.lat})</p>
                <p>Temperatura: {main.temp}</p>

            </div>
        )
    }
}
