import React from 'react';
import { BrowserRouter as Router} from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';

import PresentarTiempo from './Component/PresentarTiempo';
import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/:ciudad' component={PresentarTiempo} />
        
      </Switch>
    </Router>
    
  );
}

export default App;
