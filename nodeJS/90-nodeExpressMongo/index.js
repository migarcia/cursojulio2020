var express = require('Express');
var app = express();

var rutas = require('./rutas.js.js');

app.get('/:id', function(req, res){
    res.send('The id you specified is ' + req.params.id);
 });
 

//both index.js and things.js should be in same directory
app.use('/rutas', rutas);

app.listen(3000);