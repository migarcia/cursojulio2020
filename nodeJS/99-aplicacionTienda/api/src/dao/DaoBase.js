'use strict';


const Conexion =  require('../utiles/Conexion');
module.exports = class DaoBase {
    constructor(programa, tabla, ordenes) {
        this.programa = programa;
        this.tabla = tabla;
        this.ordenes = ordenes;
        this.con = Conexion.getInstance();
    }

    /**
     * Selecciona t6oda la tabla
     */
    async selectAll() {
        console.log('entro en SELECT ALL');
        return await this.selectComun(this.ordenes.SELECT_ALL)
    }
    /**
     * Selecciona una unica fila de la tabla
     * @param {} id  ley registro a seleccionar
     */
    async selectUno(id) {
        console.log('entro en SELECT UNO para ' + id);
        return await this.selectComun(this.ordenes.SELECT_UNO + this.con.limpia(id));
    }
    /**
     * Espera se envie una condicion WHERE tipo
     *   A=3 AND B0dddd
     * @param {*} where 
     */
    async selectVar(where) {
        console.log('entro en SELECT VAR para ' + where);
        return await this.selectComun(this.ordenes.SELECT_VAR + where);
    }
    /**
     * Busca la clave maxima añadida a la tabla
     */
    async selectMax() {
        console.log('entro en SELECT MAX');
        return await this.selectComun(this.ordenes.SELECT_MAX);
    }

    /**
     * Realiza el SELECT, comprueba que haya alguno, y devuelve las filas
     * @param {*} sql 
     */
    async selectComun(sql) {
        console.log('haciendo-' + sql);
        let rows = await this.con.query(sql);
        console.log('pasado select');
        if (rows.length < 1) {
            throw new Error('No existen registros para ->' + sql);
        }
        return rows;

    }


    /**
     * Espera el objeto correspondiente y lo inserta en la tabla
     * Devuelve el contador de registros afectados
     * @param {*} datos 
     */
    async insert(datos) {
        let result = await this.con.query(this.ordenes.INSERT, datos);
        console.log('Insert hecho');
        return result;

    }

    /**
     * Actualiza el registro cuya Primary Key se indica en id,
     * La actualizacion se hace del registro entero
     * @param {*} datos El registro a actualizar
     * @param {*} id    El Primary Key a actualizar
     */
    async update(datos, id) {
    //    id=this.con.limpia(id); 
        id=Number(id);
        console.log(id,datos);
        return  await this.con.query(this.ordenes.UPDATE, [datos, id]);
    }

    /**
     * Borra el registro indicado por la Primary key
     * @param {*} id  Valor de Primary key del registro a eliminar
     */
    async delete(id) {
        return await this.con.query(this.ordenes.DELETE + id);
    }

    /**
     * {Le indican un nombre de campo y un valor, y lo añade a la string
     * @param  *  salida  String con el trozo construido
     * @param {*} nombre  nombre a utilizar
     * @param {*} valor  valor a colocar
     * @param {*} sepa  separador a colocar en cabeza si la string de salida ya contiene algo
     */
    ponSalida(salida,nombre,valor,sepa){
        if (!salida=='') salida += ' ' + sepa + ' ';
        salida += ' ' + nombre;
        salida += ' = ';
        salida += mysql.escape(valor);
        return salida;
    }
}