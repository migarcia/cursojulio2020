'use strict';


const DaoBase = require ('./DaoBase');

const ORDENES_CAT={
    'INSERT' : 'INSERT INTO categoria SET ?',
    'SELECT_ALL' : 'SELECT * FROM categoria',
    'SELECT_UNO' : 'SELECT * FROM categoria WHERE id_categoria = ',
    'SELECT_VAR' : 'SELECT * FROM categoria WHERE ',
    'SELECT_MAX' : 'SELECT MAX(id_categoria) as idmax FROM categoria',
    'DELETE'     : 'DELETE FROM categoria WHERE id_categoria = ',
    'UPDATE'     : 'UPDATE categoria SET ? WHERE id_categoria = ?'
}
module.exports = class DaoCategoria extends DaoBase {

    /**
     * Constructor pasando al padre:
     * Nombre del modulo actual
     * Nombre de la tabla a utilizar
     * Lista de ordenes SQL
     */
    constructor(){
        super("DaoCategoria","Categoria",ORDENES_CAT);
    }

    /**
     * Espera un objeto categoria y crea una lista utilizando los campos
     * con contenido para montar una lista WHERE
     * @param {*} categoria - objeto con contenido para el WHERE
     */
    selectVar(categoria){
        salida = montaVar(categoria,' AND ');
        return super.selectVar(salida);
    }
    
    /**
     * espera recibir un objeto categoria y en funcion de que estén en blanco o no
     * va montando una lista con los valores
     * @param {*} dat 
     * @param {*} sepa 
     */
    montaVar(dat,sepa){
        let salida='';
        if (!dat.id_categoria=='' && !dat.id_categoria==0){
            salida = ponSalida(salida,'id_categoria',dat.id_categoria,sepa);
        }
        if (!dat.cat_nombre=='' ){
            salida = ponSalida(salida,'cat_nombre',dat.cat_nombre,sepa);
        }
        if (!dat.cat_descripcion=='' ){
            salida = ponSalida(salida,'cat_descripcion',dat.cat_descripcion,sepa);
        }
        return salida;
    }
}
