'use strict';

const DaoCategoria = require('../dao/DaoCategoria');
const ControllerBase = require('./ControllerBase');


module.exports = class CategoriaController extends ControllerBase {
    constructor() {
        const daoC = new DaoCategoria();
        super(daoC);
    }

    getId(objeto){
        return objeto.id_categoria;
    }
}