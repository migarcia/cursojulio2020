'use strict';

module.exports = class ControllerBase {
    constructor(dao){
        this.dao = dao;
    }
    list() {
        console.log("voy a listar categorias ");
        return  this.dao.selectAll();
    }

    listUno(id) {
        return this.dao.selectUno(id);
    }

    actualiza(objeto){
        let id = this.getId(objeto);
        console.log("actualiza",objeto);
        console.log("id:",id);
        return this.dao.update(objeto,id);
    }

    borra(objeto){
        let id = this.getId(objeto);
        console.log("borrando ",id);
        return this.dao.delete(id);
    }

    inserta(objeto) {
        console.log("inserta:",objeto);
        return this.dao.insert(objeto);
    }
}

