'use strict';

const mysql = require('mysql');
const { promisify } = require("util");
var objetoConexionx1x2x3x4x5x6x7;
module.exports = class Conexion {

    static getInstance(){
      if (objetoConexionx1x2x3x4x5x6x7==undefined){
        objetoConexionx1x2x3x4x5x6x7=new Conexion();
      }
      return objetoConexionx1x2x3x4x5x6x7;    
    
    }

    async makeConnection() {
        this.conec = mysql.createPool({
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'tienda'
        });
        // try {
        await new Promise((resolve, reject) => {
            this.conec.getConnection(function (err, connection) {
                if (err) {
                    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                        console.error('La conexion se cerro');
                    }
                    if (err.code === 'ER_CON_COUNT_ERROR') {
                        console.error('Demasiadas conexiones')
                    }
                    if (err.code === 'ECONNREFUSED') {
                        console.error('Conexion rechazada');
                    }
                    //console.log(err);
                    throw "Fallo abriendo BBDD";
                    //return reject(err);
                }
                console.log('Conectado');
                resolve(connection);
            });
        });
    }

    async getConnection() {
        if (this.conec === undefined ||
            this.conec == "") {
            console.log("entrando a make");
            await this.makeConnection();
            console.log('saliendo de make');

        }
        console.log("Pasando...");

        return this.conec;

    }

    async query( sql, args ) {
        let conexion = await this.getConnection()
        return new Promise( ( resolve, reject ) => {
          conexion.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }

    async close() {
      let conexion = await this.getConnection()
        return new Promise( ( resolve, reject ) => {
          conexion.end( err => {
                if ( err ){
                    return reject( err );
                }
                this.conec =""  //cierro mi indicador de conexion
                resolve();
            } );
        } );
    }

    limpia(valor){
        return mysql.escape(valor);
    }
}