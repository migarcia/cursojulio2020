'use strict';
//const Conexion = require('./utiles/Conexion');
const DaoCategoria = require('./dao/DaoCategoria');

//const con = Conexion.getConnection()

const daoC = new DaoCategoria();
debugger;
procesoPrueba();


/*********************FIN******************************************/


/********************************************************************
 * rutinas de ejecucion
 * Se ha montado todo esto, para permitir las pruebas en secuencia
 * merced a los awaits introducidos
 * 
 */

async function procesoPrueba(){

    //***********************SELECT*********************** */
    await pruebaSelect();

    //***********************INSERTANDO********************* */
    await pruebaInserta();

    //*******************actualizando        */
    await pruebaActualiza();

    //**************************BORRANDO******************* */
    await pruebaBorrado();
}

async function pruebaSelect() {
    console.log("Inicio Selecion");
    daoC.selectAll()
        .then(rows => {
            console.log(rows);
            console.log("Saliendo Selecion");
        })
        .catch(err => {
            console.error(err);
        });
}

async function pruebaInserta() {
    const cate = {
        id_categoria: 0,
        cat_nombre: 'aaaaaaaaaaaaaa',
        cat_descripcion: 'bbbbbbbbbb bbbbbbbbbbbbbbbbb ' + new Date()
    }

    daoC.insert(cate)
        .then(numero => {
            console.log('Se han insertado ' + numero.affectedRows);
            console.log("Saliendo insert");
            console.log("Insertado registro " + numero.insertId);

        })
        .catch(err => {
            console.error(err);
        });
}

async function pruebaActualiza() {
    let maximo = await daoC.selectMax();
    console.log(maximo);
    const cate1 = {
        id_categoria: maximo[0].idmax,                     //consigo el ultimo indice grabado
        cat_nombre: 'modif ' + new Date(),
        cat_descripcion: 'He modificado este registro'
    };
    console.log(cate1);
    daoC.update(cate1, cate1.id_categoria)
        .then(numero => {
            console.log('Modificacion de ' + cate1.id_categoria + ' Se han modificado ' + numero.affectedRows);
            daoC.selectUno(cate1.id_categoria)          // lee el registro modificado
                .then(rows => {
                    muestraCategoria(rows[0]);          // para mostrarlo
                })
                .catch(err => {
                    console.error(err);
                });
        })
        .catch(err => {
            console.error(err);
        });
}

async function pruebaBorrado() {
    let maximo = await daoC.selectMax();

    daoC.delete(maximo[0].idmax)
        .then(numero => {
            console.log('Borrado ' + maximo[0].idmax + ' Se han borrado ' + numero.affectedRows);
        })
        .catch(err => {
            console.error(err);
        });
}

function muestraCategoria(cat) {
    console.log('--------------------------------------------------------------');
    console.log(cat.id_categoria);
    console.log(cat.cat_nombre);
    console.log(cat.cat_descripcion);
}