const  express = require('express');
const  Categoria = require('../src/controller/CategoriaController');

const  router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
  console.log('Preparando...');
  cat=new Categoria();
  cat.list()
  .then(lista => res.json(lista));
 
});

router.get('/:id', function(req, res, next) {
  cat=new Categoria();
  cat.listUno(req.params.id)
  .then(lista => res.json(lista));
});

router.post('/', function(req, res, next) {
  cat=new Categoria();
  cat.inserta(montaObjeto(req))
    .then(rsp => res.send(rsp.affectedRows.toString()));
});

router.put('/', function(req, res, next) {
  cat=new Categoria();
  cat.actualiza(montaObjeto(req))
    .then(rsp => res.send(rsp.affectedRows.toString()));
});

router.delete('/', function(req, res, next) {
  cat=new Categoria();
  cat.borra(montaObjeto(req))
    .then(rsp => res.send(rsp.affectedRows.toString()));
});

function montaObjeto(req){
  let categoria= {
    id_categoria: req.body.id_categoria,
    cat_nombre: req.body.cat_nombre,
    cat_descripcion: req.body.cat_descripcion
  }
  
  return categoria;
}

module.exports = router;
