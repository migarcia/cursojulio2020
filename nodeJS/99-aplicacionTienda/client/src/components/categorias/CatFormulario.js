import React, { Component } from 'react';


export default class CatFormulario extends Component {

    constructor(props) {
        super(props)
        this.state = {
            categoria:''
        }
    }

    gestionCampo = e => {
        switch (e.target.name) {
            case 'id_categoria':
                this.setState({categoria: {id_categoria: e.target.value}});
                break;
            case 'cat_nombre':
                this.setState({categoria: {cat_nombre: e.target.value }});
                break;
            case 'cat_descripcion':
                this.setState({categoria: {cat_descripcion: e.target.value }});
                break;
            default:
                console.log(e.target.name)
                break;
        }
    }
    componentDidMount() {
        this.setState({
            categoria: this.props.categoria
        });
        console.log(this.state.categoria);
     }

    render() {
        

        return (
            <div>
                <h1>Categorias</h1>
                <form className="form-horizontal" onSubmit={this.props.controlCat}>
                    <div className="form-group row">
                        <label htmlFor="id_categoria" className="col-sm-2 control-label">Id</label>
                        <div className="col-sm-5">
                            <input type="number"
                                value={this.state.categoria.id_categoria} name="id_categoria" className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="cat_nombre" className="col-sm-2 control-label">Nombre Categoria</label>
                        <div className="col-sm-10">
                            <input type="text"
                                value={this.state.categoria.cat_nombre} name="cat_nombre" required className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="cat_descripcion" className="col-sm-2 control-label">Descripcion categoria</label>
                        <div className="col-sm-10">
                            <input type="text"
                                value={this.state.categoria.cat_descripcion} name="cat_descripcion" required className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <input type="submit"
                                value={this.props.tipoBoton} 
                                className="btn btn-default" 
                                />

                        </div>
                    </div>
                </form>
            </div>
        )
    }

}