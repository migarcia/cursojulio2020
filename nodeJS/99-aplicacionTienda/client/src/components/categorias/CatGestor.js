import React, { Component } from 'react';

import CatListado from './CatListado';

import CatFormulario from './CatFormulario';

export default class CatGestor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formulario: false,
            categoria: "",
            listado: "",
            tipoBoton: "",
            orderSend: ''
        }
        this.leerRegistro = this.leerRegistro.bind(this);
    }
    componentDidMount() {
        if (!this.state.formulario) {
            this.conseguirLista();
        }
    }

    /**accede para conseguir la lista 
     * de todas las categorias y las deja en estado
     */
    conseguirLista() {
        let apiUrl = 'http://localhost:3100/categoria'
        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState({ listado: data }))
            .catch(error => console.log(error));
    }
    /**
     * Recibe un id, y solicita el registro correspondiente
     * @param {*} id 
     */
    leerRegistro(id) {
        this.setState({ categoria: '' });
        let apiUrl = 'http://localhost:3100/categoria'
        apiUrl = apiUrl + '/' + id;
        this.setState({ categoria: '' });
        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState({ categoria: data[0] }))
            .catch(error => console.log(error));
    }

    /** 
     * se dispara al submit del fromulario; 
     * monta objeto y lo envia
     * si todo va bien, consigue nuevo listado y se pone en modo listado
     */
    controlCat = (e) => {
        e.preventDefault();
        //---_>enviar orden       
        let objeto = this.montaObjeto(e.target.elements);
        let objJSON = JSON.stringify(objeto)

        console.log(objJSON);
        let apiUrl = 'http://localhost:3100/categoria'
        fetch(apiUrl, {
            method: this.state.orderSend,
            headers: { "Content-Type": "application/json" },
            body: objJSON
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                if (data !== 1) {
                    console.log("Respuesta desde host:", data);
                } else {
                    this.setState({
                        mensaje: data[0],
                        formulario: false
                    });
                    this.conseguirLista();
                }
            })
            .catch(error => console.log(error));
    }

    /** cuando recibe la orden de borrar
     * activa el modo formulario
     * 
     */
    bborrar = (id) => {
        this.setState({
            formulario: true,
            tipoBoton: "Borrar",
            orderSend: "DELETE"
        })
        this.leerRegistro(id);
    }

    bact = (id) => {
        this.setState({
            formulario: true,
            tipoBoton: "Actualizar",
            orderSend: "PUT"

        })
        this.leerRegistro(id);
    }
    insert = () => {
        this.setState({
            formulario: true,
            tipoBoton: "Insertar",
            orderSend: "POST",
            categoria: {
                id_categoria: 0,
                cat_nombre: "",
                cat_descripcion: ""
            }

        })

    }

    montaObjeto = dato => {
        let objeto = {
            id_categoria: dato.id_categoria.value,
            cat_nombre: dato.cat_nombre.value,
            cat_descripcion: dato.cat_descripcion.value
        }
        return objeto
    }
    render() {

        return (
            <>
                {
                    this.state.formulario ?
                        this.state.categoria !== "" ?
                            <CatFormulario
                                categoria={this.state.categoria}
                                control={this.props.control}
                                controlCat={this.controlCat}
                                tipoBoton={this.state.tipoBoton} />
                            : <h1>Accediendo a registro</h1>
                        :
                        this.state.listado !== "" ?
                            <CatListado control={this.props.control}
                                listado={this.state.listado}
                                bborrar={this.bborrar}
                                bact={this.bact}
                                binsert={this.insert} />
                            :
                            <h1>Cargando</h1>

                }
            </>
        )
    }

}