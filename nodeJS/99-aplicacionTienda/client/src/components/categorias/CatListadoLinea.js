import React, { Component } from 'react';

import Boton from '../Boton.js';


/**
 * Recibe elemento con un objeto categoria
 * bborrar  Nombre de la rutina para borrar
 * bact nombre de la rutina para actualizar
 */

export default class CatListadoLinea extends Component {

    render(){
        let id = this.props.elemento.id_categoria;

        return(
            <tr key={this.props.elemento.id_categoria}>
                <td>{this.props.elemento.id_categoria}</td>
                <td>{this.props.elemento.cat_nombre}</td>
                <td>{this.props.elemento.cat_descripcion}</td>
                <td><Boton  nombre={"Modificar"} ejec={this.props.bact} valorId={id} /></td>
                <td><Boton  nombre={"Borrar"} ejec={this.props.bborrar} valorId={id} /></td>
            </tr>
        )
    }
}