import React, { Component } from 'react';
import CatListadoCabecera from './CatListadoCabecera';
import CatListadoLinea from './CatListadoLinea';
/**
 * bborrar  Nombre de la rutina para borrar
 * bact nombre de la rutina para actualizar
 */

export default class CatListado extends Component {

    constructor(props) {
        super(props)
        this.state = {};
    }

    render() {

        return (
            <>
            <h1>Listado Categorias <button onClick={this.props.binsert}>Add</button></h1>
            <table>
                <thead>
                    <CatListadoCabecera />
                </thead>
                <tbody>
                {this.props.listado.map((element, key) => {
                    return <CatListadoLinea key={key} elemento={element} bborrar={this.props.bborrar} bact={this.props.bact} />
                })}
                </tbody>
            </table>
            </>
        )
    }
}