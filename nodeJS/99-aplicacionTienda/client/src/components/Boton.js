import React, { Component } from 'react';

/**
 * ejec con la rutina a llamar
 * nombre con el texto del boton
 */
export default class Boton extends Component {

    render(){
        return (
            <button onClick={e=>this.props.ejec(this.props.valorId)} >{this.props.nombre}</button> 
        )
    }
}