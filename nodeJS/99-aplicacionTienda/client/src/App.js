import React from 'react';
import GestorForm  from './components/gestorForm';
import './App.css';

function App() {
  return (
    <div className="App">
     <GestorForm />
    </div>
  );
}

export default App;
