const FACTOR_MILLAS_KM = 1.60934;
module.exports = {
    
    m2km: function (millas) {
        return millas * FACTOR_MILLAS_KM;
    },

    km2m: function (km) {
        return km / FACTOR_MILLAS_KM;
    },

    c2f: function (c) {
        let cToF = c * 9 / 5 + 32;
        return cToF;
    },

    f2c: function (f) {
        let fToCel = (f - 32) * 5 / 9;
        return fToCel
    }
}