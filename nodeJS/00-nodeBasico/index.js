
const func = require('./conversiones');



let centigrados = 30;
console.log(centigrados + ' °C. son ' + func.c2f(centigrados) + ' ℉ ');

let fahrenheit = 86;
console.log(fahrenheit + ' ℉ son ' + func.f2c(fahrenheit) + ' °C. ');

let millas = 1609;
console.log(millas + ' millas son ' + func.m2km(millas) + " Km.");

let kilometros = 1000;
console.log(kilometros + ' kilometros son ' + func.km2m(kilometros) + " Millas");