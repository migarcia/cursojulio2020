const http = require('http');
const qs = require('querystring');          //<----------------------------------
const func = require('./conversiones');

const hostname = 'localhost';
const port = 3006;
const informe = {
    celsiusI: 0,
    fahrenheitO: ""
}


var formulario = `<form method="POST" action="/procesar">
<h1>Los datos</h1> 
<fieldset>
<label>Temperatura (C)</label>
<input type="number" name="centigrados" value={{1}} /> Pues mi respuesta es que  {{2}}
</fieldset>
<input type="submit" value="Enviar" />
</form>`;

/**
 * Creacion de las opciones y metodos del servidor
 */
const server = http.createServer((req, res) => {

    if (req.method == "GET") {
        dialogoGET(req, res)
    } else {
        if (req.method == 'POST') {
            dialogoPOST(req, res)
        } else {
            console.log(req.headers);
            res.statusCode = 500;
            res.setHeader('Content-Type', 'text/html');
            res.end('Metodo desconocido=' + req.method);
        }
    }

})
/**
 * Arranque del servidor
 */
server.listen(port, hostname, () => {
    console.log(`Servidor activo en http://${hostname}:${port}/`);
});

function dialogoGET(req, res) {
    if ('/f2c' == req.url) {
        enviaFormulario(res, informe);
    }
}

function dialogoPOST(req, res) {

    console.log("POST reconocido");
    var body = [];

    req.on('data', trozo => {
        body.push(trozo);
        console.log("trozo=" + trozo);
    }).on('end', () => {
        body = Buffer.concat(body).toString()
    });
    console.log("body=" + body);
    req.on('end', function () {
        console.log(body);
        if ('/procesar' == req.url) {
            console.log("Url reconocido");
            informe.celsiusI = qs.parse(body).centigrados;
            informe.fahrenheitO = "Convertido a F, es: " + func.c2f(informe.celsiusI);

            enviaFormulario(res, informe);
        }

    })
}

function enviaFormulario(res, informe) {
    res.write("<html><header></header><body>")

    console.log(informe);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    salida = formulario;
    salida = salida.replace("{{1}}", informe.celsiusI);
    salida = salida.replace("{{2}}", informe.fahrenheitO);
    res.write(salida);
    res.write("</body></html")
    res.end();
}
