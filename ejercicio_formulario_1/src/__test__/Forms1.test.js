import React from 'react';
import { mount, shallow } from 'enzyme'; 

import Forms1 from '../components/formularios/form1/Forms1';
 
describe('Mi formulario', () => {
  let formulario
  beforeEach(() => {
    formulario = mount(<Forms1  />);
    });
    afterEach(() => {
      formulario.unmount();
    });


    it('Debe visualizarse como siempre', () => {
      const component = shallow(<Forms1  />);   
      expect(component).toMatchSnapshot();
    });

    it('Aceptar props', () => {
        const props = {
            name:"Miguel Garcia",
            email:"migarcia@dopc.com"
        }
        const wrapper = mount(<Forms1 {...props} />);
        const form = wrapper.find('form');
        
        const input1 = form.find('#name');
        console.log(input1.debug())
        expect(input1.props().value).toBe(props.name)
        const input2 = form.find('#email');
        expect(input2.props().value).toBe(props.email)
        const button = form.find('button');
        button.simulate('click');
        wrapper.unmount();
      });
  });