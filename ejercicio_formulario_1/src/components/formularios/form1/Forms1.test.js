import React from "react";
import { render } from '@testing-library/react';
import renderer from 'react-test-renderer';

import Forms1 from "./Forms1";


let input = null;
let obj;
const pNombre = 'Miguel Garcia Garcia';
const pNombreMontado = 'Miguel,Garcia,Garcia,';
const pNombreDefault = 'un nombre';
const pListaLabel = ['Nombre', 'Correo Electronico', 'Su sabor favorito:'];
let eventoChkTrue = {
  target: {
    type:"checkbox",
    checked:"true",
    value:"",
    id:"isActive"
  }
}
let eventoNombre = {
  target: {
    type:"Text",
    checked:"true",
    value:pNombre,
    id:"name"
  }
}

describe("Comprueba inputs", () => {
  it("Entrada checked", () =>{
    obj.gestionInputCambia(eventoChkTrue);
    expect(obj.state.isActive).toBe(true);
  })
  it("Entrada nombre",() => {
    obj.gestionInputCambia(eventoNombre);
    expect(obj.state.name).toBe(pNombre);
  })
})
/* deja en utils el formulario renderizado*/
function montado() {
  return render(<Forms1 />);
}

/* deja en utils el formulario renderizado pasandole un nombre*/
function montadoConNombre() {
 return render(<Forms1 name={pNombre} />)
}


beforeEach(() => {
  const component = renderer.create(<Forms1 />)
  obj = component.getInstance();
  
})

describe("Revision funciones", () => {
  it("Probar montaNombre 3 palabras", () => {
    //expect(obj.montarNombre(pNombre)).toBe(pNombreMontado);
    expect(obj.montarNombre("Esto es un nombre largo")).toBe("Esto,es,un,nombre,largo,");
  })
  it("Probar montaNombre 1 palabras", () => {
    expect(obj.montarNombre("palabra")).toBe("palabra,");
    
  })
  it("Probar montaNombre blanco", () => {
    expect(obj.montarNombre("")).toBe(",");
  })
  it("Probar montaNombre null", () => {
    expect(obj.montarNombre(null)).toBe(",");
  })
})
describe("Presentaciones", () => {
  it('Se visualiza bien', () => {
    const imagen = renderer.create(<Forms1 name={pNombre} />)
      .toJSON();
    expect(imagen).toMatchSnapshot();
  });
  pListaLabel.forEach(etiqueta => {
    it('Comprobar que existe campo ' + etiqueta, () => {
      let utils= montado();   
      const label = utils.getByLabelText(etiqueta);
      expect(label).toBeInTheDocument();
    })
  })
})
describe("Comportamiento de datos", () => {
  it('ver el nombre vacio', () => {
    let utils = montado();
    const entrada = utils.getByLabelText("Nombre");
    expect(entrada.value).toBe(pNombreDefault)
  })
  it('ver el nombre lleno', () => {
    let utils = montadoConNombre();
    const entrada = utils.getByLabelText("Nombre");
    expect(entrada.value).toBe(pNombre)
    let estado = obj.controlTest()  //?????????????
    expect(estado.name).toBe(pNombre)
    expect(obj.state.name).toBe(pNombre)
  })
})




let container = null;
/* beforeEach(() => {
  // Preparar una area del DOM
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // limpieza al salir
  unmountComponentAtNode(container);
  container.remove();
  container = null;
}); */
/* describe("Prueba presentacion", () => {

it("Probar la presentacion", () => {
    act(() => {
        container = ReactTestUtils.renderIntoDocument(<Forms1 />);
        const h4 = ReactTestUtils.findRenderedDOMComponentWithTag(
            container, 'h4'
          );
      //render(<Forms1 />, container);
    });
    //expect(container.textContent).toBe("Formulario");

    act(() => {
      render(<Forms1 name="Miguel" />, container);
    });

    expect(container.textContent).toBe("Miguel");

    act(() => {
      render(<Forms1 email="migarcia@dopc.com" />, container);
    });
    expect(container.textContent).toBe("migarcia@dopc.com");
  });

}) */