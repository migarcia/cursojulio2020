import React, { Component } from 'react'

export default class Forms1 extends Component {

    static defaultProps = {                 //utilizacion de props por defecto
        name: "un nombre",
        email: "",
        isActive: false,
        fruta: "mango"
    }

    constructor(props) {
        super(props)
        this.state = { ...this.props }
    }

    montarNombre = (nombre) => {
        let salida = "";
        if (nombre !== null) {
            const entrada = nombre.split(' ');
            entrada.forEach(dato => {
                salida += dato + ",";
            })
        }
        return salida;
    }
    /**
     * Esto es un ejemplo de documentacion
     * @param {*} e - evento
     */
    gestionEnvio = (e) => {
        e.preventDefault();
        console.log(this.montarNombre(this.state.name));
        console.log(this.state);
    }

    gestionInputCambia = (e) => {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        const nombreCampo = target.id;
        //console.log(target, value)

        this.setState({
            [nombreCampo]: value
        });
    }

    render() {
        const { name, email, isActive, fruta } = this.state;
        return (
            <div>
                <h4>Formulario</h4>
                <form>
                    <div className="form-group">
                        <label htmlFor='name'>Nombre</label>
                        <input
                            type="text"
                            className="form-control"
                            id='name'
                            placeholder='introduce el nombre'
                            value={name}
                            onChange={this.gestionInputCambia}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor='email'>Correo Electronico</label>
                        <input
                            type="text"
                            className="form-control"
                            id='email'
                            name='email'
                            placeholder='introduce el Coreo electronico'
                            value={email}
                            onChange={this.gestionInputCambia}
                        />
                    </div>
                    <div className="form-group">
                        Activado:
                        <input
                            id='isActive'
                            type="checkbox"
                            checked={isActive}
                            onChange={this.gestionInputCambia}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="fruta">
                            Su sabor favorito:
                            </label>
                        <select value={fruta}
                            id="fruta"
                            onChange={this.gestionInputCambia}
                        >
                            <option value="fresas">Fresas</option>
                            <option value="limon">Limon</option>
                            <option value="naranjas">Naranjas</option>
                            <option value="mango">Mango</option>
                        </select>

                    </div>

                    <button type="submit" className="btn btn-primary"
                        onClick={this.gestionEnvio}
                    >Enviar</button>
                </form>
            </div>
        )
    }
}