import React, { Component } from 'react'
import PropTypes from 'prop-types';

const ANIMALES={
    aguila: 'https://www.anipedia.net/imagenes/reproduccion-del-aguila-imperial-oriental.jpg',
    ardilla:'https://www.anipedia.net/imagenes/ardilla-sobre-arbol.jpg',
    ballena: 'https://www.anipedia.net/imagenes/ballenas-2.jpg',
    cocodrilo: 'https://www.anipedia.net/imagenes/cocodrilos-fotos.jpg'
    }

export default class ImagenAnimal extends Component {
    lista = 
        static propTypes = {
            animal: PropTypes.oneOf(['aguila','ardilla','ballena','cocodrilo'])
        }
        static defaultProps = {
            animal:'cocodrilo'
        }

        state = {src:ANIMALES[this.props.animal]}


        render () {
            return (
                <div>
                <p>Esto es un {this.props.animal}</p>
                <img
                    alt={this.props.animal}
                    src={this.state.src}
                    width='250'
                    />
                </div>
            )
        }
    }


