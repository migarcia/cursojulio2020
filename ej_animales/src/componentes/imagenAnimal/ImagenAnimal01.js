import React, { Component } from 'react'
import PropTypes from 'prop-types';

import ANIMALES from '../data/animales.js';



export default class ImagenAnimal01 extends Component {
    static propTypes = {
        animal: PropTypes.oneOf(Object.keys(ANIMALES))
    }
    static defaultProps = {
        animal: 'cocodrilo'
    }
    constructor(props) { 
        super(props)
        this.state = { src: ANIMALES[this.props.animal] }
    }
componentWillReceiveProps(nextProps) {
    this.setState({ src: ANIMALES[nextProps.animal] })
}

componentDidUpdate(prevProps, prevState){
    const img = document.querySelector('img');
    img.animate([
        {
            filter: 'blur(2px)'
        },
        {
            filter: 'blur(opx)'
        }
    ], {
        duration: 1500,
        easing: 'ease'
    });
}

render() {

    return (
        <div>
            <h2>Visor de animales</h2>

            <p>Esto es un {this.props.animal}</p>
            <img
                alt={this.props.animal}
                src={this.state.src}
                width='550'
            />
        </div>
    )
}
    }


