import React, { Component } from 'react'
import ImagenAnimal01 from './ImagenAnimal01'
import ANIMALES from '../data/animales.js';



export default class GestorImagenes extends Component{
    state = {animal:'cocodrilo'}
    render() {
        const lista = Object.keys(ANIMALES);
        
        return(
            <div>
            <h2> Ejemplo ComponentWillReceive</h2>
            {lista.map(nombreAnimal => (
                <button 
                    key={nombreAnimal}
                    onClick={() => this.setState({animal:nombreAnimal})}
                    disabled={nombreAnimal===this.state.animal}
                >
                {nombreAnimal}
                </button>
            ))}
            <ImagenAnimal01 animal={this.state.animal}/>
            </div>
        )
    }
}
