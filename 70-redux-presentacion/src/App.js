import React from 'react';
import {store} from './componentes/redux/Store';
import {anadirTarea} from './componentes/redux/Acciones';

import './App.css';

function visualizaEstado(cuando){
  const estado = store.getState().lista;
  console.log(estado)
  return `El valor ${cuando} es : ${estado}`
}

function App() {
  window.store = store;
  store.subscribe(() => console.log('El store se modifica'));
  const antes = visualizaEstado("Antes");
  store.dispatch(anadirTarea('Trabajo a realizar'));
  const despues = visualizaEstado("despues")
  store.dispatch(anadirTarea('Mucho mas'));
  const masDespues = visualizaEstado("masDespues")
  return (
    <div className="App">
      <header className="App-header">
        
        <p>Valor antes {antes}</p>
        <p>Valor despues {despues}</p>
        <p>Valor despues {masDespues}</p>
        
      </header>
    </div>
  );
}

export default App;
