import {ADD} from "../constantes/tipos-accion";

export function anadirTarea(tarea) {
    return { type: ADD,
             payload: tarea
    }
}
