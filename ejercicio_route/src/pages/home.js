import React from 'react';
import Menu from '../components/menu';

const href="https://medium.com/@simonhoyos/creemos-un-blog-con-react-y-c%C3%B3mo-enrutar-nuestra-aplicaci%C3%B3n-con-react-router-parte-ii-44835476bbcf";

const Home = () => (
    
    <section>
        <Menu />
        <h3>Desde Recursos Formacion</h3>
        <p>Nuestra aplicacion con React y Route</p>
        <p>Otro ejemplo <a href={href}>aqui</a></p>
    </section>
);

export default Home;