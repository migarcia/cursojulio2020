class Casa {
    ventanas = 0;
    habitaciones = 0;

    static contador = 0

    static destruye(num) {
        casas[num] = 'delete'
    }

    abrirCasa() {
        console.log('Casa abierta' + ventanas);
    }
}
Casa.contador = 2;
Casa.destruye(2);

let micasa = new Casa();
let tucasa = new Casa();

micasa.ventanas = 5;
micasa.habitaciones = 3;

tucasa.ventanas = 10;
tucasa.habitaciones = 4;

console.log(micasa.ventanas);     //----->5
console.log(tucasa.ventanas);    //----->10

micasa.contador = 15;
console.log(tucasa.contador)     //------>
console.log(Casa.ventanas);
micasa.