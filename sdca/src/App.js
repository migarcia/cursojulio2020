import React from 'react';
import logo from './logo.svg';
import './App.css';
import FPaises from './Components/Formularios/FPaises';

const pais={
  country_id:3,
  iso2:'AL',
  short_name:'Albania',
  spanish_name:'Albania',
  calling_code: null,
  cctld: null,
  iso3:null,
  long_name:null,
  numcode:0,
  un_member:0
}

function App() {
  return (
    <div>
    <FPaises elemento = {pais} />
    </div>
  );
}

export default App;
