import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class FPaises extends Component{


    constructor(props){
        super(props);
        console.log(this.props);
        this.state={
            pais: this.props.elemento
        } 
        this.enviaFormulario = this.enviaFormulario.bind(this);
        
    }
enviaFormulario(e){
    e.preventDefault();
    this.setState({contador:45});
    console.log(this.state);
}

    render() {
        let {country_id,iso2,short_name,spanish_name,calling_code,cctld,iso3,long_name,numcode,un_member} = this.state.pais;
        let cPais = this.state.pais;
        return (
            <div className="container">
            <form>
                <div className="md-form row mb-3">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="country_id">id</label>
                    <input type="text"
                            id="country_id" 
                            className="form-control-plaintext col  validate" 
                            value={country_id}
                            readOnly
                            />
                </div>
                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="isp2">iso2</label>
                    <input type="text" 
                            id="iso2" 
                            className="form-control-plaintext validate col"
                            value={iso2}
                            onChange={e=>{
                                cPais.iso2 = e.target.value;
                                this.setState({pais:cPais})
                            }}
                            />
            </div>
                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="short_name">Nombre corto</label>
                    <input type="text" 
                            id="short_name" 
                            className="form-control-plaintext validate col"
                            value={short_name}
                            onChange={e=>this.setState({short_name:e.target.value})}
                            />
                </div>

                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="spanish_name">Nombre en castellano</label>
                    <input type="text" 
                            id="spanish_name" 
                            className="form-control-plaintext validate col"
                            value={spanish_name}
                            onChange={e=>this.setState({spanish_name:e.target.value})}
                            />
                </div>

                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="calling_code">Codigo de llamada</label>
                    <input type="text" 
                            id="calling_code" 
                            className="form-control-plaintext validate col"
                            value={calling_code||0}
                            onChange={e=>this.setState({calling_code:e.target.value})}
                            />
                </div>

                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="cctld">CCTLD</label>
                    <input type="text" 
                        id="cctld" 
                        className="form-control-plaintext validate col"
                        value={cctld||0}
                        onChange={e=>this.setState({cctld:e.target.value})}
                        />
                </div>
                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="iso3">Iso3</label>
                    <input type="text"
                            id="iso3" 
                            className="form-control-plaintext validate col"
                            value={iso3||iso2}
                            onChange={e=>this.setState({iso3:e.target.value})}/>                                                                                   
                </div>
                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="long_name">Nombre largo</label>
                    <input type="text" 
                            id="long_name" 
                            className="form-control-plaintext validate col"
                            value={long_name||short_name}
                            onChange={e=>this.setState({long_name:e.target.value})}
                            />
                </div>
                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="numcode">Código numerico</label>
                    <input type="text" 
                            id="numcode" 
                            className="form-control-plaintext validate col"
                            value={numcode}
                            onChange={e=>this.setState({numcode:e.target.value})}
                            />
                </div>
                <div className="md-form mb-3 row">
                    <label className="col-sm-4 col-form-label" data-error="wrong" data-success="right" htmlFor="un_member">un_member</label>
                    <input type="text" 
                            id="un_member" 
                            className="form-control-plaintext validate col"
                            value={un_member}
                            onChange={e=>this.setState({un_member:e.target.value})}
                            />
                </div>
                <div className="modal-footer  justify-content-center">
                    <button className="btn btn-unique" onClick={this.enviaFormulario} id="botonEnvio">Enviar</button>
                </div>
            </form>
            </div>

        )
    }
}
FPaises.propTypes = {
    elemento:PropTypes.shape({
        country_id:PropTypes.number,
        iso2: PropTypes.string.isRequired,
        short_name: PropTypes.string.isRequired
    })
}