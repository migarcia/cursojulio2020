//DANIEL PINO

class Moto {
    constructor(marca, km, cc, precio)
    {
        this.marca = marca;
        this.km = km; 
        this.cc = cc;
        this.precio = precio;
    }
}

let lista = [
    new Moto("HONDA", 38000, 50, 1100),
    new Moto("HONDA", 38000, 100, 1100),
    new Moto("HONDA", 50000, 200, 1100),
    new Moto("DUCATI", 47000, 300, 1100),
    new Moto("HONDA", 27000, 230, 1100),
    new Moto("DUCATI", 35600, 240, 1000),
    new Moto("YAMAHA", 12000, 60, 1500),
    new Moto("SUZUKI", 200000, 120, 1200),
    new Moto("TRIUMPH", 150000, 300, 1000),
    new Moto("VESPA", 170000, 100, 1300),
    new Moto("YAMAHA", 172000, 230, 40),
    new Moto("HARLEY", 17200, 380, 2000),
]

let elementos = lista.length; // Número de elementos de la lista (número de motos)

listaPrecio = lista.sort(comparaPrecio); // Lista ordenada por precio ascendente;
listaNombre = lista.filter(p=> p.marca == "YAMAHA"); //Lista de motos de marca Yamaha
listaHonda30Kkms = lista.filter(p=>p.marca =="HONDA").filter(p=>p.km>=30000);
lista30Kkms240cc = lista.filter(p=>p.cc>240).filter(p=>p.km<=30000);
listaMoto = lista.filter(p=>p.cc>350).filter(p=>p.km<=25000).filter(p=>1800<p.precio<2200);

console.log("La mas barata es: " + listaPrecio[0].marca);
console.log("La mas cara es: " + listaPrecio[elementos-1].marca);
console.log(listaNombre);
console.log(listaHonda30Kkms);
console.log(lista30Kkms240cc);
console.log(listaMoto);

function comparaPrecio(a,b){
    if(a.precio===b.precio) return 0;
    if(a.precio>b.precio) return 1;
    else return -1;
}

