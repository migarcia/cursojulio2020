//import Moto from './moto.js';
//const Moto = require('./moto.js');
class Moto {

    // precio;
    // km;
    // marca;
    // cc;

    constructor(marca, km, cc, precio) {
        this.marca = marca;
        this.km = km;
        this.cc = cc;
        this.precio = precio;
    }
}


let lista = [
    new Moto("HONDA", 3000, 500, 10000),
    new Moto("HONDA", 5000, 1000, 5000),
    new Moto("HONDA", 35000, 350, 2500),
    new Moto("HONDA", 10000, 1000, 4500),
    new Moto("HONDA", 25000, 750, 1954),
    new Moto("HONDA", 17324, 359, 2710),
    new Moto("HONDA", 8932, 1500, 15500),
    new Moto("HONDA", 40745, 500, 14440),
    new Moto("HONDA", 59678, 2000, 1100),
    new Moto("DUCATI", 3000, 500, 10000),
    new Moto("DUCATI", 5000, 1000, 5000),
    new Moto("DUCATI", 35000, 350, 2500),
    new Moto("DUCATI", 10000, 1000, 4500),
    new Moto("DUCATI", 25000, 750, 1954),
    new Moto("BMW", 17324, 359, 2710),
    new Moto("BMW", 8932, 1500, 15500),
    new Moto("BMW", 40745, 500, 14440),
    new Moto("BMW", 59678, 2000, 1100),

]
//console.log(lista);
let elementos = lista.length;
let porPrecio = lista.sort(sortPrecio);

console.log("la mas barata es " + porPrecio[0].marca + " a " + editPrecio(porPrecio[0].precio));
console.log("la mas cara es " + porPrecio[elementos - 1].marca + " a " + editPrecio(porPrecio[elementos - 1].precio));

let cantidadNombres = lista.reduce((contador, unaMoto) => {
    //console.log("Entra:" + unaMoto.marca + "=>" + (contador[unaMoto.marca] || 0));
    contador[unaMoto.marca] = (contador[unaMoto.marca] || 0) + 1;
    //console.log("Sale:" + unaMoto.marca + "=>" + (contador[unaMoto.marca] || 0));
    return contador;
}, {});
console.log(cantidadNombres);

totalPrecio = lista.reduce((acumulador, unaMoto) => {
    //console.log("Entra:" + unaMoto.marca + "=>" + acumulador);
    acumulador = (acumulador || 0 ) + unaMoto.precio + ",";
    //console.log("sale:" + unaMoto.marca + "=>" + acumulador);
    return acumulador;
});
console.log(totalPrecio);
salida=[];
for (moto of lista) {
    salida[moto.marca] = (salida[moto.marca]|| 0) + 1;
}
console.log(salida);

function editPrecio(precio) {
    return precio.toLocaleString('es-ES', { minimumFractionDigits: 2 });
}

function sortPrecio(a, b) {
    if (a.precio == b.precio) return 0;
    if (a.precio < b.precio) return -1;
    return 1;
}