export default class Moto {
    
     precio = 0;
     km = 0;
     marca = "";
     cc = "";

     constructor(marca,km,cc,precio){
         this.marca=marca;
         this.km = km;
         this.cc = cc;
         this.precio = precio;
     }
}