import React, { Component } from 'react';
import Formulario1 from './Componentes/formulario/Formulario1';
export default class Alarma extends Component {
    estado = this.props.isActive ? 'si' : 'no';

    render() {
        return (
            <div>
                <Formulario1>Este formulario viene de alarma</Formulario1>
                <p>Estado: {this.estado}</p>
                <p>{this.props.mensaje}</p>
                <p> El valor es {this.props.valor}</p>
                <p>Lista-{this.props.lista.join(', ')}</p>
                <p>{this.props.children}</p>
            </div>
        )
    }
}
Alarma.defaultProps = {
    valor: 0,
    lista: []
}