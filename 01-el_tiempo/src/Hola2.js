import React, { Component } from "react";
import Interruptor from './Interruptor';
class Hola2 extends Component {

    dia() {
        return "semana";
    }

    saluda(event){
        console.log(event);
        alert("Buenos dias");
        let nativo = event.nativeEvent;
        console.log(nativo);
    }
    render() {
        let texto = this.dia();
        return (
            <div>
                <Interruptor estadoInicial
                    texto='Esto es el texto del interruptors'
                />
                <h2>Y esto desde clases, {texto}</h2>
                <button onClick={this.saluda}>Saludo</button>
            </div>
        )
    }
}

export default Hola2