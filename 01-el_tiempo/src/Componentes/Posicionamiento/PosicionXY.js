import React,{Component} from 'react';

export default class PosicionXY extends Component {

    constructor(){
        super();
        this.state = {mouseX:0,
                      mouseY:0};
        this.gestionMouseMove = this.gestionMouseMove.bind(this);
        this.gestionOnClick = this.gestionOnClick.bind(this);

    }
    gestionMouseMove(e){
        const {clientX, clientY} = e;
        this.setState({
            mouseX:clientX,
            mouseY:clientY
        })
    }
    gestionOnClick(e){
        alert(this.state.mouseX + "x" + this.state.mouseY);
    }

    render(){
        return (
            <div
            onMouseMove = {this.gestionMouseMove}
            onClick = {this.gestionOnClick}
            style={{
                border:'1px solid black',
                marginTop: 10,
                padding: 10,
            
            }}>
            <h1>{this.props.titulo}</h1>
            <p>{this.state.mouseX} , {this.state.mouseY}</p>
            </div>
        )
    }

}