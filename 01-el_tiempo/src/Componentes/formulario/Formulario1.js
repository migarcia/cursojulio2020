import React, { Component } from 'react';

export default class Formulario1 extends Component {

    constructor() {
        super();
     //   this.inputNombre = React.createRef();
        this.state = {
            name: 'Miguel garcia',
            email: 'migarcia@recursosformacion.com',
            isActive: true
        }
    }

    gestionEnvio = (e) => {
        e.preventDefault();
        // const nombre = this.inputNombre.current.value;
        // const email = document.getElementById('email').value;
        console.log(this.state);
    }


    render() {
        const { name, email, isActive } = this.state;
        return (
            <div>
                <form>
                    <h4>Formulario</h4>
                    <h5>{this.props.children}</h5>
                    <div className="form-group">
                        <label htmlFor='name'>Nombre</label>
                        <input className="form-control"
                            id='name'
                            name='name'
                            placeholder='Introduce el nombre'

                            value={name}
                            onChange={e=>this.setState({name:e.target.value})}
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor='email'>Correo Electrónico</label>
                        <input className="form-control"
                            id='email'
                            name='email'
                            placeholder='Introduce el correo electrónico' 
                            value = {email}
                            onChange={e=>this.setState({email:e.target.value})}
                            />
                    </div>

                    <button type='submit'
                        className='btn btn-primary'
                        onClick={this.gestionEnvio}>Enviar</button>
                </form>
            </div>
        )
    }
}