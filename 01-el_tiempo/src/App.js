import React from 'react';
import logo from './logo.svg';
import './App.css';
import Hola2 from './Hola2';
import Alarma from './Alarma';
import PosicionXY from './Componentes/Posicionamiento/PosicionXY';
import Formulario1 from './Componentes/formulario/Formulario1';
function Hola(props) {
  return <div>
    <Hola2 />
    <h1>Y seguimos con los holas, {props.autor}</h1>
  </div>
}



function App() {
  return (
    <div className="App">
    <PosicionXY titulo="Esto es el titulo"/>
    <Formulario1 >
    <div>Hola</div>
    Este formulario se emplea para pedir los datos de la gente conectada
    </Formulario1>

      <Hola autor='Miguel' />
      <Hola autor='Pepe' />
      <Alarma isActive={false}
        mensaje='Esto es una alarma'
        valor={3}

      >
      Este mensaje es para Alarma
      </Alarma>
      
      <Alarma isActive
        mensaje='Esto es otra alarma'
        valor={3}
        lista = {['casa','perro','gato']}

      />
      
    </div>
  );
}

export default App;
