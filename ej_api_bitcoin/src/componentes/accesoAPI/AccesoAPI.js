import React, { Component } from 'react'
/* obteniendo datos de https://www.coindesk.com/coindesk-api */

export default class AccesoAPI extends Component {
                      
    static API_URL = 'https://api.coindesk.com/v1/bpi/currentprice.json';
    static API_URL_HISTORIC = 'https://api.coindesk.com/v1/bpi/historical/close.json'
// se puede añadir ?start=2013-09-01&end=2013-09-05, si no, son los 31 dia precedente

    state = {bpi:{}}
function sumar(a,b){ return a+b};
c=sumar(1+2)
// Se solicita la informacion
    componentDidMount() {
        fetch(AccesoAPI.API_URL)
            .then(res => res.json())
            .then(data => {
                const {bpi} = data;
                this.setState({bpi })
            });
    }

    visualizarCambio(){
        const {bpi} = this.state;
        const monedas = Object.keys(bpi);
        return monedas.map(divisa => (
            <div key={divisa}>
            1 BTC es {bpi[divisa].rate}
            &nbsp;
            <span>{bpi[divisa].description}</span>
            </div>
        ))
    }

    render() {
        return (
            <div>
                <h2>Cotizacion bitcoin</h2>
                {this.visualizarCambio()}
            </div>
        )
    }
}