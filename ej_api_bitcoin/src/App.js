import React from 'react';

import './App.css';
import AccesoAPI from './componentes/accesoAPI/AccesoAPI.js';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <AccesoAPI/>
      </header>
    </div>
  );
}

export default App;
