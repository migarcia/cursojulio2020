import React, { Component } from 'react';


export default class CatFormulario extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id_categoria: 0,
            cat_nombre: "",
            cat_descripcion: "",
            tipoBoton: "Enviar"

        }
    }

    gestionCampo = e => {
        switch (e.target.name) {
            case 'id_categoria':
                this.setState({ id_categoria: e.target.value })
                break;
            case 'cat_nombre':
                this.setState({ cat_nombre: e.target.value })
                break;
            case 'cat_descripcion':
                this.setState({ cat_descripcion: e.target.value })
                break;
            default:
                console.log(e.target.name)
                break;
        }
    }
    componentDidMount() { }

    render() {

        return (
            <div>
                <h1>Clientes 1</h1>
                <form onSubmit={this.props.envia1} className="form-horizontal">

                    <div className="form-group row">
                        <label htmlFor="id_categoria" className="col-sm-2 control-label">Id</label>
                        <div className="col-sm-5">
                            <input type="number"
                                value={this.state.id_categoria} name="id_categoria" className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="cat_nombre" className="col-sm-2 control-label">Nombre cliente</label>
                        <div className="col-sm-10">
                            <input type="text"
                                value={this.state.cat_nombre} name="cat_nombre" required className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="cat_descripcion" className="col-sm-2 control-label">Direccion</label>
                        <div className="col-sm-10">
                            <input type="text"
                                value={this.state.cat_descripcion} name="cat_descripcion" required className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <input type="submit" value={this.state.tipoBoton} className="btn btn-default" />

                        </div>
                    </div>
                </form>
            </div>
        )
    }

}