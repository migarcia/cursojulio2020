import React, { Component } from 'react';

import Formulario1 from './Formulario1';
import RespuestaPostMan from './../Utiles/RespuestaPostMan';

export default class FormularioClientes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listaClientes: [],
            posicion: 0,
            data: null
        }
    }

    

    finFormulario = e => {
        e.preventDefault();

        const { cliente_id, cliente_nombre, cliente_direccion1 } =
            e.target.elements;
        let cliente = {
            cliente_id: cliente_id.value,
            cliente_nombre: cliente_nombre.value,
            cliente_direccion1: cliente_direccion1.value
        }
        // let llevo = this.state.listaClientes;
        // llevo.push(cliente);
        // this.setState({ listaClientes: llevo });
        console.log(this.state.listaClientes);
        this.comunicaciones(cliente)
        this.props.control(false);
        //this.props.cambio();

    }

    comunicaciones = datos => {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        const url = "https://postman-echo.com/post"
        fetch(proxyurl + url, {
            method: 'POST',
            //mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache'
            },
            body: JSON.stringify(datos),
        })
            .then(result => result.json())
            .then(result => {
                this.setState({
                    data: result
                })
                console.log(this.state.data)
            })

    }

    render() {
        
        let todos = this.state.listaClientes;
        let posicion = this.state.posicion;
        let cliente = {
            cliente_id: 0,
            cliente_nombre: "",
            cliente_direccion1: ""
        }

        if (posicion < todos.length) {
            cliente = {
                cliente_id: todos[posicion].client_id,
                cliente_nombre: todos[posicion].cliente_nombre,
                cliente_direccion1: todos[posicion].cliente_direccion1
            }
        }
        cliente = {
            cliente_id:12 ,
            cliente_nombre: 'MMMMMMMMM',
            cliente_direccion1: 'kkkkkkkfgfhgfhgkkkkkkk'
        }
        return (
            <div>
                <Formulario1 final={this.finFormulario} cliente={cliente} control={this.props.control}/>

                {this.state.data != null ?
                <RespuestaPostMan valor={this.state.data} /> : null}
           </div>
        )
    }

}