import React, { Component } from 'react';


export default class Formulario1 extends Component {

    constructor(props) {
        super(props)
        this.state = {
            cliente_id: 0,
            cliente_nombre: "",
            cliente_direccion1: "",
            tipoBoton: "Enviar"

        }
    }

    gestionCampo = e => {
        // switch (e.target.name) {
        //     case 'cliente_id':
        //         this.setState({ cliente_id: e.target.value })
        //         break;
        //     case 'cliente_nombre':
        //         this.setState({ cliente_nombre: e.target.value })
        //         break;
        //     case 'cliente_direccion1':
        //         this.setState({ cliente_direccion1: e.target.value })
        //         break;
        //     default:
        //         console.log(e.target.name)
        //         break;
        // }
        this.props.control(true);
        this.setState({ [e.target.name]: e.target.value });
    }
    componentDidMount() {
        this.setState({...this.props.cliente})
        console.log("didmount");
     }

    


    render() {

        return (
            <div>
                <h1>Clientes 1</h1>
                <form onSubmit={this.props.final} className="form-horizontal">

                    <div className="form-group row">
                        <label htmlFor="cliente_id" className="col-sm-2 control-label">Id</label>
                        <div className="col-sm-5">
                            <input type="number"
                                value={this.state.cliente_id} name="cliente_id" className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="cliente_nombre" className="col-sm-2 control-label">Nombre cliente</label>
                        <div className="col-sm-10">
                            <input type="text"
                                value={this.state.cliente_nombre} name="cliente_nombre" required className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="cliente_direccion1" className="col-sm-2 control-label">Direccion</label>
                        <div className="col-sm-10">
                            <input type="text"
                                value={this.state.cliente_direccion1} name="cliente_direccion1" required className="form-control"
                                onChange={(e) => this.gestionCampo(e)}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <input type="submit" value={this.state.tipoBoton} className="btn btn-default" />

                        </div>
                    </div>
                </form>
            </div>
        )
    }

    componentWillUnmount() {
        window.alert("Me voy");
    }
}