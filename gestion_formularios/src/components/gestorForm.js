import React, { Component } from 'react';

import FormularioClientes from './clientes/FormularioClientes';
import FormularioCategorias from './categorias/FormularioCategorias';
import FormularioProductos from './productos/FormularioProductos';

export default class GestorForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            f1: false,
            f2: false,
            f3: false,
            mod: false
        }
    }
    activaMod= (estat) => this.setState({mod : estat});
  

    // enMod = () => {
    //     if (this.state.mod) {
    //         return window.confirm("Las modificaciones no se han guardado.\n ¿Quiere cambiar?");
    //     };
    //     return true;
    // }


    // actF1 = e => {
    //     if (this.enMod) {
    //         this.setState({
    //             f1: true,
    //             f2: false,
    //             f3: false
    //         });
    //     }
    // }
    // actF2 = e => {
    //     if (!this.enmod) {
    //         this.setState({
    //             f1: false,
    //             f2: true,
    //             f3: false
    //         });
    //     }
    // }
    // actF3 = e => {
    //     if (this.enMod) {
    //         this.setState({
    //             f1: false,
    //             f2: false,
    //             f3: true
    //         });
    //     }
    // }

    botonera = e => {
        if (
        (this.state.mod &&
             window.confirm("Las modificaciones no se han guardado.\n ¿Quiere cambiar?")) ||
         !this.state.mod) {
            this.setState({
                f1:false,
                f2:false,
                f3:false,
                mod:false
            });
            this.setState({[e.target.name]:true});
            
        }
    }



    render() {
        return (
            <div>
                <button name="f1" onClick={this.botonera}>Formulario clientes</button>
                <button name="f2" onClick={this.botonera}>Formulario categorias</button>
                <button name="f3" onClick={this.botonera}>Formulario productos</button>

                {this.state.f1 ? <FormularioClientes control={this.activaMod} /> : null}
                {this.state.f2 ? <FormularioCategorias control={this.activaMod}  /> : null}
                {this.state.f3 ? <FormularioProductos control={this.activaMod}  /> : null}
            </div>
        )

    }
}