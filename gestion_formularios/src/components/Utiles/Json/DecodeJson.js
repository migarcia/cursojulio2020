import React, { Component } from 'react';


export default class DecodeJson extends Component {

  isString(x) {

    return Object.prototype.toString.call(x) === "[object String]";
  }

  isObject(x) {

    return Object.prototype.toString.call(x) === "[object Object]";
  }
  render() {
    let dato = this.props.valor;
    let salida = dato;
    console.log(Object.prototype.toString.call(dato))
    
    if (this.isObject(dato)) {
      console.log('entrando');
        salida += this.desmontaObject(dato);
    } else {
      salida += dato;
    }
    console.log(salida);


    return (
      <div>
        {this.props.texto} : {salida}
      </div>

    )
  }

  getKeys = function (obj) {
    var keys = [];
    for (var key in obj) {
      keys.push(key);
    }
    return keys;
  }
  desmontaObject = obj => {
    let salida = "";
    let lista = this.getKeys(obj);
    for (let key in lista) {
      if (this.isObject(lista[key])) {
        console.log('mas-' + key)
        salida += this.desmontaObject(lista[key])
      } else {
        if (this.isArray(lista[key])) {
          salida += lista[key]
        }
        salida += key + ":" + lista[key] + ",";
      }
    }
    return salida;
  }
}