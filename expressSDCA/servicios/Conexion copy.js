/**
 * Conexion -  Realizar conexion con base de datos
 * leertabla     - devuelve listado entero de la tabla indicada
 */
const mysql = require('mysql');

class Conexion {

    static conexion() {
        Conexion.pool = mysql.createPool({
            connectionLimit: 100,
            debug: false,
            host: 'localhost',
            user: 'root',
            password: 'garcia1',
            database: 'contabilidadautonomos'
        });
        //  this.conn.connect((err) => {
        //     if (err) throw err;
        //     console.log('Connected!');
        // });
    }


    static leerTabla(tabla) {
        if (Conexion.pool == null) Conexion.conexion();
        // let conta = 0;
        // while (Conexion.pool == null) {conta++;}
        // console.log(conta);

        return new Promise((resolve, reject) => {
            Conexion.pool.getConnection((err, conn) => {
                if (err) reject(err);


                console.log('connected as id ' + conn.threadId);
                let sql = `SELECT * FROM ${tabla}`;
                return new Promise((resolve, reject) => {
                    conn.query(sql, (err, datos) => {
                        conn.release();
                        if (err) reject(err);
                        //console.log(datos);
                        resolve(datos);
                        //console.log("paso")

                    })

                });
            });
        })
    }


    // if (respuesta.isValid) {
    //     return respuesta;
    // }else {
    //     console.log("Error acceso------>", respuesta);
    // }


}

module.exports = Conexion;
