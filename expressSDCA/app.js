/**
 * aplicacion express
 * 
 */

'use strict'

const express = require('express');
//var bodyParser = require('body-parser');
const app = express();

// Importamos las rutas
const rComunidades = require('./rutas/rutaComunidades'); 
const rPais = require('./rutas/rutaPaises'); 
const rPoblacion = require('./rutas/rutaPoblacion'); 
const rProvincia = require('./rutas/rutaProvincia'); 

//cargar middlewares
//Configuramos bodyParser para que convierta el body de nuestras peticiones a JSON
//app.use(bodyParser.urlencoded({extended:false}));
//app.use(bodyParser.json());
// Cargamos las rutas
app.use('/api/comunidades', rComunidades);
app.use('/api/paises', rPais);
app.use('/api/poblacion', rPoblacion);
app.use('/api/provincia', rProvincia);
// exportamos este módulo para poder usar la variable app fuera de este archivo
module.exports = app;