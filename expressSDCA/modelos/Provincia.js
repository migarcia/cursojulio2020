
class Provincia {
    // cppro_id;
    //cppro_nombre;
    //cppro_codca;
    //cppro_capital;

    constructor(cppro_id, cppro_nombre, cppro_codca, cppro_capital) {
       
        this.cppro_id = cppro_id;
        this.cppro_nombre = cppro_nombre;
        this.cppro_codca = cppro_codca;
        this.cppro_capital = cppro_capital;
    }
}
module.exports = Provincia;