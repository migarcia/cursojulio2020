/**
 * Controlador para DatosCliente
 * recibe llamadas para editar, añadir, listar y borrar comunidades
 */
const ControladorBase = require("./ControladorBase");

const MODELO = require("../modelos/DatosCliente");
const TABLA = 'datoscliente';
const SELECT_ALL = `SELECT * from ${TABLA} WHERE id_customer = :customer`;
const SELECT_UNO = `SELECT * FROM ${TABLA} WHERE cppro_id = :id AND id_customer = :customer`;
const SELECT_SELECT = `SELECT id_cliente as id,dc_nombre as opcion FROM ${TABLA} WHERE id_customer = :customer`;

class ControladorPoblacion extends ControladorBase {
    constructor(){
        let config = {
            TABLA:TABLA,
            SELECT_UNO : SELECT_UNO,
            SELECT_SELECT: SELECT_SELECT,
            MODELO:MODELO,
            campoId: 'cppro_id',
        }
        super(config);
    }
    

    
}

// const ccaa = new ControladorCCAA()
// ccaa.listado()



module.exports = ControladorDatosCliente;