/**
 * Controlador para Poblaciones
 * recibe llamadas para editar, añadir, listar y borrar comunidades
 */
const ControladorBase = require("./ControladorBase");

const MODELO = require("../modelos/Provincia");
const TABLA = 'cp_provincias';
const SELECT_UNO = `SELECT * FROM ${TABLA} WHERE cppro_id = :id`
const SELECT_SELECT = `SELECT cppro_id as id,cppro_nombre as opcion FROM ${TABLA} WHERE cppro_codca = :id`

class ControladorPoblacion extends ControladorBase {
    constructor(){
        let config = {
            TABLA:TABLA,
            SELECT_UNO : SELECT_UNO,
            SELECT_SELECT: SELECT_SELECT,
            MODELO:MODELO,
            campoId: 'cppro_id',
        }
        super(config);
    }
    

    
}

// const ccaa = new ControladorCCAA()
// ccaa.listado()



module.exports = ControladorPoblacion;