'use strict'

var express = require('express');

var ControladorDatosClientes = require('../controladores/ControladorDatosClientes');
let dcli = new ControladorDatosClientes();
// Llamamos al router
var rutas = express.Router();
//var md_auth = require('../middlewares/authenticated');
// Creamos una ruta para los métodos que tenemos en nuestros controladores
rutas.get('/', dcli.leerAll);
rutas.get('/select',dcli.leerSelect)
rutas.get('/:id', dcli.leerUno);
// Exportamos la configuración
module.exports = rutas;