import React from 'react';
import Listar from './componentes/listado/index';
import Form from './componentes/formulario/Form';
import './App.css';

function App() {
  return (
    <div>
      <h2>Tareas</h2> 
      <Listar />
      <Form />
    </div>
  );
}

export default App;
