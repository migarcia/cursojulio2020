import {ADD} from "../constantes/tipos-accion";

const prohibido = ["fiesta", "urgente", "trabajo"];



export function controlarPalabras({ dispatch }) {
    return function(next){
      return function(action){
        //----------------------------------------
        if (action.type === ADD) {
        
            const foundWord = prohibido.filter(word =>
              action.payload.includes(word)
            );
    
            if (foundWord.length) {
              return dispatch({ type: "FOUND_BAD_WORD" });
            }
          }
        //--------------------------------------
        return next(action);
      }
    } 
  }
