import {createStore ,applyMiddleware } from "redux";

import {Reducer} from './Reducer';
import {controlarPalabras} from "./controlarPalabras"

export const store = createStore(Reducer,
                                 applyMiddleware(controlarPalabras)
                                 );

