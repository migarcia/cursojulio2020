
import {ADD} from "../constantes/tipos-accion";

const estadoBase = {
    lista: []
};

export function Reducer( state = estadoBase, action) {
    console.log("reducer:" + action.payload);
    if (action.type === ADD) {
        return Object.assign({}, state, {
            lista: state.lista.concat(action.payload)
          });
    }
    return state;
}

  