import React, { Component } from "react";
import { connect } from "react-redux";
import { anadirTarea } from "../redux/Acciones";
import PropTypes from 'prop-types';

function mapDispatchToProps(dispatch) {
  return {
    anadirTarea: datos => dispatch(anadirTarea(datos))
  };
}

class ConnectedForm extends Component {

  static propTypes={
    anadirTarea: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      tarea: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value })
    console.log(this.state);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { tarea } = this.state;
    console.log(tarea);
    this.props.anadirTarea( tarea );
    this.setState({ tarea: "" });
  }
  render() {
    const { tarea } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <label htmlFor="tarea">Tarea a realizar</label>
          <input
            type="text"
            id="tarea"
            value={tarea}
            onChange={this.handleChange}
          />
        </div>
        <button type="submit">Guardar</button>
      </form>
    );
  }
}

const Form = connect(
  null,
  mapDispatchToProps
)(ConnectedForm);

export default Form;