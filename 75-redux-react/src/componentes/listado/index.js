import React from "react";
import { connect } from "react-redux";

const select = datos => {
  return { lista: datos.lista }; 
};

const ConnectedList = ({ lista }) => (
  <ul>
    {lista.map((el,i) => (
      <li key={i}>{el}</li>
    ))}
  </ul>
);

const Listar = connect(select)(ConnectedList);
export default Listar;